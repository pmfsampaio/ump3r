#ifndef _SYSTEM_H
#define _SYSTEM_H

#define OSCILLATOR_CLOCK_FREQUENCY  12000000      //in MHz

void StartOsc(void);
unsigned int GetCclk(void);
unsigned int GetPclk(void);

#endif
