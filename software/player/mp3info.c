/*
 * mp3info.c
 *
 *  Created on: 15 Mar 2011
 *      Author: psampaio
 */
#include <stdio.h>
#include <string.h>
#include "fat/pff.h"
#include "fat/integer.h"
#include "mp3info.h"

#define MIN_CONSEC_GOOD_FRAMES 4
#define FRAME_HEADER_SIZE 4
#define MIN_FRAME_SIZE 21
#define NUM_SAMPLES 4

static int filePos;

int frame_size_index[] = { 24000, 72000, 72000 };

const int frequencies[3][4] = { { 22050, 24000, 16000, 50000 }, /* MPEG 2.0 */
{ 44100, 48000, 32000, 50000 }, /* MPEG 1.0 */
{ 11025, 12000, 8000, 50000 } /* MPEG 2.5 */
};

const int bitrate[2][3][14] = { { /* MPEG 2.0 */
{ 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256 }, /* layer 1 */
{ 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160 }, /* layer 2 */
{ 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160 } /* layer 3 */
},

{ /* MPEG 1.0 */
{ 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448 }, /* layer 1 */
{ 32, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384 }, /* layer 2 */
{ 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320 } /* layer 3 */
} };

int x_fgetc(FILINFO *f) {
	unsigned char c[2];
	unsigned int res;

	if (pf_read(/*f,*/ c, 1, &res) != FR_OK)
		return EOF;
	return (res != 1) ? EOF : c[0];
}

int header_bitrate(Mp3HeaderDsc *h) {
	return bitrate[h->version & 1][3 - h->layer][h->bitrate - 1];
}

int header_frequency(Mp3HeaderDsc *h) {
	return frequencies[h->version][h->freq];
}

int frame_length(Mp3HeaderDsc *header) {
	return header->sync == 0xFFE ? (frame_size_index[3 - header->layer]
			* ((header->version & 1) + 1) * header_bitrate(header)
			/ header_frequency(header)) + header->padding : 1;
}

int sameConstant(Mp3HeaderDsc *h1, Mp3HeaderDsc *h2) {
	if ((*(unsigned int*) h1) == (*(unsigned int*) h2))
		return 1;

	if ((h1->version == h2->version) && (h1->layer == h2->layer) && (h1->crc
			== h2->crc) && (h1->freq == h2->freq) && (h1->mode == h2->mode)
			&& (h1->copyright == h2->copyright) && (h1->original
			== h2->original) && (h1->emphasis == h2->emphasis))
		return 1;
	else
		return 0;
}

/* Remove trailing whitespace from the end of a string */
char *unpad(char *string) {
	char *pos = string + strlen(string) - 1;
	while (pos[0] == ' ')
		(pos--)[0] = 0;
	return string;
}

int get_id3(Mp3InfoDsc *mp3) {
	int retcode = 0;
	char fbuf[4];
	unsigned int readed;
	FRESULT res;

	if (mp3->datasize >= 128) {
		if (pf_lseek(/*&(mp3->file),*/ mp3->datasize - 128)) {
			//	   fprintf(stderr,"ERROR: Couldn't read last 128 bytes of %s!!\n",mp3->filename);
			retcode |= 4;
		} else {
			//fread(fbuf, 1, 3, mp3->file);
			res = pf_read(/*&(mp3->file),*/ fbuf, 3, &readed);
			fbuf[3] = '\0';
			mp3->id3.genre[0] = 255;

			if (!strcmp((const char *) "TAG", (const char *) fbuf)) {

				mp3->id3_isvalid = 1;
				mp3->datasize -= 128;
				pf_lseek(/*&(mp3->file),*/ mp3->file.fsize - 125);//, SEEK_END);
				//fread(mp3->id3.title, 1, 30, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.title, 30, &readed);
				mp3->id3.title[30] = '\0';
				//fread(mp3->id3.artist, 1, 30, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.artist, 30, &readed);
				mp3->id3.artist[30] = '\0';
				//fread(mp3->id3.album, 1, 30, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.album, 30, &readed);
				mp3->id3.album[30] = '\0';
				//fread(mp3->id3.year, 1, 4, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.year, 4, &readed);
				mp3->id3.year[4] = '\0';
				//fread(mp3->id3.comment, 1, 30, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.comment, 30, &readed);
				mp3->id3.comment[30] = '\0';
				if (mp3->id3.comment[28] == '\0') {
					mp3->id3.track[0] = mp3->id3.comment[29];
				}
				//fread(mp3->id3.genre, 1, 1, mp3->file);
				pf_read(/*&(mp3->file),*/ mp3->id3.genre, 1, &readed);
				unpad(mp3->id3.title);
				unpad(mp3->id3.artist);
				unpad(mp3->id3.album);
				unpad(mp3->id3.year);
				unpad(mp3->id3.comment);
			}
		}
	}
	return retcode;

}

/* Get next MP3 frame header.
 Return codes:
 positive value = Frame Length of this header
 0 = No, we did not retrieve a valid frame header
 */

int GetHeader(FILINFO *file, Mp3HeaderDsc *header) {
	unsigned char buffer[FRAME_HEADER_SIZE];
	int fl;
	unsigned int readed;
	FRESULT res;

	res = pf_read(/*file,*/ buffer, FRAME_HEADER_SIZE, &readed);

	//	if (fread(&buffer, FRAME_HEADER_SIZE, 1, file) < 1) {
	if (readed < 1) {
		header->sync = 0;
		return 0;
	}
	header->sync = (((int) buffer[0] << 4) | ((int) (buffer[1] & 0xE0) >> 4));
	if (buffer[1] & 0x10)
		header->version = (buffer[1] >> 3) & 1;
	else
		header->version = 2;
	header->layer = (buffer[1] >> 1) & 3;
	if ((header->sync != 0xFFE) || (header->layer != 1)) {
		header->sync = 0;
		return 0;
	}
	header->crc = buffer[1] & 1;
	header->bitrate = (buffer[2] >> 4) & 0x0F;
	header->freq = (buffer[2] >> 2) & 0x3;
	header->padding = (buffer[2] >> 1) & 0x1;
	header->extension = (buffer[2]) & 0x1;
	header->mode = (buffer[3] >> 6) & 0x3;
	header->mode_extension = (buffer[3] >> 4) & 0x3;
	header->copyright = (buffer[3] >> 3) & 0x1;
	header->original = (buffer[3] >> 2) & 0x1;
	header->emphasis = (buffer[3]) & 0x3;

	return ((fl = frame_length(header)) >= MIN_FRAME_SIZE ? fl : 0);
}

int Mp3FirstHeader(Mp3InfoDsc *mp3, long startpos) {
	int k, l = 0, c;
	Mp3HeaderDsc h, h2;
	long valid_start = 0;

	filePos = startpos;
	pf_lseek(/*&(mp3->file),*/ startpos);
		while ((c = x_fgetc(&(mp3->file))) != 255 && (c != EOF))
			filePos++;
		filePos++;
		if (c == 255) {
			filePos--;
			pf_lseek(/*&(mp3->file),*/ filePos);
			valid_start = filePos;
			if ((l = GetHeader(&(mp3->file), &h))) {
				filePos += FRAME_HEADER_SIZE;
				pf_lseek(/*&(mp3->file),*/ filePos + (l - FRAME_HEADER_SIZE));
				filePos += (l - FRAME_HEADER_SIZE);
				for (k = 1; (k < MIN_CONSEC_GOOD_FRAMES) && (mp3->datasize
						- filePos >= FRAME_HEADER_SIZE); k++) {
					if (!(l = GetHeader(&(mp3->file), &h2))) {
						filePos += FRAME_HEADER_SIZE;
						break;
					}
					if (!sameConstant(&h, &h2))
						break;
					filePos += FRAME_HEADER_SIZE;
					filePos += (l - FRAME_HEADER_SIZE);
					pf_lseek(/*&(mp3->file),*/ filePos);
				}
				if (k == MIN_CONSEC_GOOD_FRAMES) {
					pf_lseek(/*&(mp3->file),*/ valid_start);
					filePos = valid_start;
					memcpy(&(mp3->header), &h2, sizeof(Mp3HeaderDsc));
					mp3->header_isvalid = 1;
					return 1;
				}
			}
			else {
				filePos += FRAME_HEADER_SIZE;
			}
		} else {
			return 0;
		}

	return 0;
}

/* get_next_header() - read header at current position or look for
 the next valid header if there isn't one at the current position
 */
int Mp3NextHeader(Mp3InfoDsc *mp3) {
	int l = 0, c, skip_bytes = 0;
	Mp3HeaderDsc h;

	while (1) {
		while ((c = x_fgetc(&(mp3->file))) != 255 && ((filePos + 1)
				< mp3->datasize)) {
			skip_bytes++;
			filePos++;
		}
		filePos++;
		if (c == 255) {
			filePos--;
			pf_lseek(/*&(mp3->file),*/ filePos);
			if ((l = GetHeader(&(mp3->file), &h))) {
				if (skip_bytes)
					mp3->badframes++;
				filePos += FRAME_HEADER_SIZE;
				filePos += (l - FRAME_HEADER_SIZE);
				pf_lseek(/*&(mp3->file),*/ filePos);
				return 15 - h.bitrate;
			} else {
				filePos += FRAME_HEADER_SIZE;
				skip_bytes += FRAME_HEADER_SIZE;
			}
		} else {
			if (skip_bytes)
				mp3->badframes++;
			return 0;
		}
	}
	return 0;
}

int Mp3Info(Mp3InfoDsc *mp3, int scantype, int fullscan_vbr) {
	int had_error = 0;
	int frame_type[15] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	float seconds = 0, total_rate = 0;
	int frames = 0, frame_types = 0, frames_so_far = 0;
	int l, vbr_median = -1;
	int bitrate, lastrate;
	int counter = 0;
	Mp3HeaderDsc header;
	off_t sample_pos, data_start = 0;

	get_id3(mp3);
	if (scantype == SCAN_QUICK) {
		if (Mp3FirstHeader(mp3, 0L)) {
			data_start = filePos;
			lastrate = 15 - mp3->header.bitrate;
			while ((counter < NUM_SAMPLES) && lastrate) {
				sample_pos = (counter * (mp3->datasize / NUM_SAMPLES + 1))
						+ data_start;
				if (Mp3FirstHeader(mp3, sample_pos)) {
					bitrate = 15 - mp3->header.bitrate;
				} else {
					bitrate = -1;
				}

				if (bitrate != lastrate) {
					mp3->vbr = 1;
					if (fullscan_vbr) {
						counter = NUM_SAMPLES;
						scantype = SCAN_FULL;
					}
				}
				lastrate = bitrate;
				counter++;

			}
			if (!(scantype == SCAN_FULL)) {
				mp3->frames = (mp3->datasize - data_start) / (l = frame_length(
						&mp3->header));
				mp3->seconds = (int) ((float) (frame_length(&mp3->header)
						* mp3->frames) / (float) (header_bitrate(&mp3->header)
						* 125) + 0.5);
				mp3->vbr_average = (float) header_bitrate(&mp3->header);
			}
		}

	}

	if (scantype == SCAN_FULL) {
		if (Mp3FirstHeader(mp3, 0L)) {
			data_start = filePos;
			while ((bitrate = Mp3NextHeader(mp3))) {
				frame_type[15 - bitrate]++;
				frames++;
			}
			memcpy(&header, &(mp3->header), sizeof(Mp3HeaderDsc));
			for (counter = 0; counter < 15; counter++) {
				if (frame_type[counter]) {
					frame_types++;
					header.bitrate = counter;
					frames_so_far += frame_type[counter];
					seconds += (float) (frame_length(&header)
							* frame_type[counter]) / (float) (header_bitrate(
							&header) * 125);
					total_rate += (float) ((header_bitrate(&header))
							* frame_type[counter]);
					if ((vbr_median == -1) && (frames_so_far >= frames / 2))
						vbr_median = counter;
				}
			}
			mp3->seconds = (int) (seconds + 0.5);
			mp3->header.bitrate = vbr_median;
			mp3->vbr_average = total_rate / (float) frames;
			mp3->frames = frames;
			if (frame_types > 1) {
				mp3->vbr = 1;
			}
		}
	}
	return had_error;
}

