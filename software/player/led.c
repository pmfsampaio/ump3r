/*
 * led.c
 *
 *  Created on: 13 Mar 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "ump3r.h"
#include "led.h"

void LEDInit(void)
{
	IOSET1 = LED_BLUE | LED_RED | LED_GREEN;
	IODIR1 |= LED_BLUE | LED_RED | LED_GREEN;
}

void LEDBlue(int state)
{
	if (state)
		IOCLR1 = LED_BLUE;
	else
		IOSET1 = LED_BLUE;
}

void LEDGreen(int state)
{
	if (state)
		IOCLR1 = LED_GREEN;
	else
		IOSET1 = LED_GREEN;
}

void LEDRed(int state)
{
	if (state)
		IOCLR1 = LED_RED;
	else
		IOSET1 = LED_RED;
}

void LEDAllOff()
{
	IOSET1 = LED_BLUE | LED_RED | LED_GREEN;
}
