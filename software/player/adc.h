/*
 * adc.h
 *
 *  Created on: 21 Feb 2011
 *      Author: psampaio
 */

#ifndef ADC_H_
#define ADC_H_

void ADCInit(void);
unsigned int ADCRead(int channelId);

#endif /* ADC_H_ */
