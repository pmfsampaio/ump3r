/*
 * ssp.c
 *
 *  Created on: 25 Feb 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "startosc.h"
#include "ump3r.h"
#include "ssp.h"

#define SSP_SR_TFE      (1 << 0)
#define SSP_SR_TNF      (1 << 1)
#define SSP_SR_RNE      (1 << 2)
#define SSP_SR_RFF      (1 << 3)
#define SSP_SR_BSY      (1 << 4)

#define SSP_CR1_SSE     (1 << 1)

#define SSP_FIFO_DEPTH  8

static unsigned long _sspFreq;
static unsigned long _systemFreq;
static unsigned long _sspCr0;

#if 0
void sspChipSelect (int select)
{
	if (select)
	IOCLR0 = (1 << 20);
	else
	{
		IOSET0 = (1 << 20);
		while (!(SSPSR & SSP_SR_TNF));
		SSPDR = 0xff;
		// Wait until TX fifo and TX shift buffer are empty, discard any received data
		while (SSPSR & SSP_SR_BSY);
		while (!(SSPSR & SSP_SR_RNE));
		do
		{
			select = SSPDR;
		}
		while (SSPSR & SSP_SR_RNE);
	}
}
#endif

static inline void SSPSetClockFreq(unsigned int frequency) {
	unsigned int div;

	if (_sspFreq == frequency) return;
	div = _systemFreq/frequency;
	if (div < 2)
		div = 2;
	else if (div > 254)
		div = 254;

	++div;
	div &= ~1;
	SSPCPSR = div;
	_sspFreq = _systemFreq / div;
}

void SSPInit(void) {
	unsigned int i;
	volatile unsigned int dummy;

	PINSEL1 &= ~((3 << 2) | (3 << 4) | (3 << 6));
	PINSEL1 |= ((2 << 2) | (2 << 4) | (2 << 6));

	PCONP |= (1 << 10);

	_sspCr0 = SSPCR0;
	_sspFreq = (_systemFreq = GetPclk())/ SSPCPSR;
	for (i = 0; i < SSP_FIFO_DEPTH; i++)
		dummy = SSPDR;
}

inline void SSPWaitReady(void)
{
	while (!(SSPSR & SSP_SR_TFE))
		;
	while ((SSPSR & SSP_SR_BSY) != 0);
}

inline void SSPSetMode(int dataLen, int frameFormat, unsigned int frequency) {
	unsigned int dummy, i;

	if ((_sspCr0 == (dataLen | frameFormat) && _sspFreq == frequency)) return;

//	if ((SSPCR0 == (dataLen | frameFormat))) return;
	while (!(SSPSR & SSP_SR_TFE));
	while ((SSPSR & SSP_SR_BSY) != 0);
	SSPCR1 = 0x00;
	SSPCR0 = dataLen | frameFormat;
	SSPIMSC = 0x00;
	SSPSetClockFreq(frequency);
	SSPCR1 |= SSP_CR1_SSE;
	for (i = 0; i < SSP_FIFO_DEPTH; i++)
		dummy = SSPDR;
}

inline unsigned short SSPTransferByte(unsigned short c) {
	while (!(SSPSR & SSP_SR_TNF))
		;
	SSPDR = c;
	while (!(SSPSR & SSP_SR_RNE))
		;
	return SSPDR;
}

inline unsigned short SSPSendByte(unsigned short c) {
	while (!(SSPSR & SSP_SR_TNF))
		;
	SSPDR = c;
	while ((SSPSR & SSP_SR_BSY))
		;
	return SSPDR;
}




void SSPSendBlock(const unsigned char *pData, unsigned int size) {
	volatile unsigned int dummy;

	while (size--) {
		while (!(SSPSR & SSP_SR_TNF))
			;
		SSPDR = *pData++;
		while (!(SSPSR & SSP_SR_RNE))
			;
		dummy = SSPDR;
	}
}

void SSPReceiveBlock(unsigned char *pData, unsigned int size) {
	unsigned int delta = 0;

	while (size || delta) {
		while ((SSPSR & SSP_SR_TNF) && (delta < SSP_FIFO_DEPTH) && size) {
			SSPDR = 0xff;
			--size;
			++delta;
		}
		while (SSPSR & SSP_SR_RNE) {
			*pData++ = SSPDR;
			--delta;
		}
	}
}
