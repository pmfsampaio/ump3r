/*
 * sysclk.c
 *
 *  Created on: 16 Feb 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "startosc.h"
#include "vic.h"
#include "ump3r.h"
#include "sysclk.h"

unsigned long __ticks;

int SCKInit(void) {
	unsigned int pclk;

	pclk = GetPclk();

	__ticks = 0;
	T0TCR = 2;
	T0EMR = 0;
	T0PR = (pclk / 10000) - 1;
	T0MCR = 0;
	T0TCR = 1;
	return 0;
}

unsigned int GetTicks(void) {
	static unsigned int __lastTickRead = 0;
	unsigned int now = T0TC;

	__ticks += now - __lastTickRead;
	__lastTickRead = now;
	return __ticks;
}

inline unsigned int GetElapsedTicks(unsigned int startTime) {
	return GetTicks() - startTime;
}

void DelayMs(unsigned int duration) {
	unsigned int startTime = GetTicks();

	while (GetElapsedTicks(startTime) < (duration * 10))
		;
}

void Delay100us(unsigned int duration) {
	unsigned int startTime = GetTicks();

	while (GetElapsedTicks(startTime) < duration)
		;

}

int SetSecond(int match) {
	unsigned int pclk;

	pclk = GetPclk();

	T1TCR = 2;
	T1EMR = 0;
	T1PR = (pclk / 1) - 1;
	T1MCR = 0x4;
	T1MR0 = match;
	T1TC = 0;
	T1TCR = 1;

	return 0;
}

unsigned int GetSeconds(void) {
	return T1TC;
}
