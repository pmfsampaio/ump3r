/*
 * mp3info.h
 *
 *  Created on: 15 Mar 2011
 *      Author: psampaio
 */

#ifndef MP3INFO_H_
#define MP3INFO_H_

enum SCANTYPE { SCAN_NONE, SCAN_QUICK, SCAN_FULL };

typedef struct {
	unsigned long	sync;
	unsigned int	version;
	unsigned int	layer;
	unsigned int	crc;
	unsigned int	bitrate;
	unsigned int	freq;
	unsigned int	padding;
	unsigned int	extension;
	unsigned int	mode;
	unsigned int	mode_extension;
	unsigned int	copyright;
	unsigned int	original;
	unsigned int	emphasis;
} Mp3HeaderDsc;

typedef struct {
	char title[31];
	char artist[31];
	char album[31];
	char year[5];
	char comment[31];
	unsigned char track[1];
	unsigned char genre[1];
} Mp3Id3TagDsc;

typedef struct {
	FILINFO file;
	off_t datasize;
	int header_isvalid;
	Mp3HeaderDsc header;
	int id3_isvalid;
	Mp3Id3TagDsc id3;
	int vbr;
	float vbr_average;
	int seconds;
	int frames;
	int badframes;
} Mp3InfoDsc;

int Mp3Info(Mp3InfoDsc *mp3,int scantype, int fullscan_vbr);


#endif /* MP3INFO_H_ */
