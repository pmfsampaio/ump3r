/*
 * usbsys.h
 *
 *  Created on: 16 de Jul de 2011
 *      Author: psampaio
 */

#ifndef USBSYS_H_
#define USBSYS_H_

void UsbSysInit(void);
void USBHwISR(void);

#endif /* USBSYS_H_ */
