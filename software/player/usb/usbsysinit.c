/*
 * usbsysinit.c
 *
 *  Created on: 16 de Jul de 2011
 *      Author: psampaio
 */
#include "usbapi.h"
#include "msc_bot.h"
#include "blockdev.h"

#define MAX_PACKET_SIZE	64

#define LE_WORD(x)		((x)&0xFF),((x)>>8)

static U8 abClassReqData[4];

static const U8 abDescriptors[] = {

// device descriptor
	0x12,
	DESC_DEVICE,
	LE_WORD(0x0200),		// bcdUSB
	0x00,					// bDeviceClass
	0x00,					// bDeviceSubClass
	0x00,					// bDeviceProtocol
	MAX_PACKET_SIZE0,		// bMaxPacketSize
	LE_WORD(0xFFFF),		// idVendor
	LE_WORD(0x0003),		// idProduct
	LE_WORD(0x0100),		// bcdDevice
	0x01,					// iManufacturer
	0x02,					// iProduct
	0x03,					// iSerialNumber
	0x01,					// bNumConfigurations

// configuration descriptor
	0x09,
	DESC_CONFIGURATION,
	LE_WORD(32),			// wTotalLength
	0x01,					// bNumInterfaces
	0x01,					// bConfigurationValue
	0x00,					// iConfiguration
	0xC0,					// bmAttributes
	0x32,					// bMaxPower

// interface
	0x09,
	DESC_INTERFACE,
	0x00,					// bInterfaceNumber
	0x00,					// bAlternateSetting
	0x02,					// bNumEndPoints
	0x08,					// bInterfaceClass = mass storage
	0x06,					// bInterfaceSubClass = transparent SCSI
	0x50,					// bInterfaceProtocol = BOT
	0x00,					// iInterface
// EP
	0x07,
	DESC_ENDPOINT,
	MSC_BULK_IN_EP,			// bEndpointAddress
	0x02,					// bmAttributes = bulk
	LE_WORD(MAX_PACKET_SIZE),// wMaxPacketSize
	0x00,					// bInterval
// EP
	0x07,
	DESC_ENDPOINT,
	MSC_BULK_OUT_EP,		// bEndpointAddress
	0x02,					// bmAttributes = bulk
	LE_WORD(MAX_PACKET_SIZE),// wMaxPacketSize
	0x00,					// bInterval

// string descriptors
	0x04,
	DESC_STRING,
	LE_WORD(0x0409),

	0x0E,
	DESC_STRING,
	'L', 0, 'P', 0, 'C', 0, 'U', 0, 'S', 0, 'B', 0,

	0x0c, //12,
	DESC_STRING,
	'u', 0, 'M', 0, 'P', 0, '3', 0, 'r', 0, //'c', 0, 't', 0, 'X', 0,

	0x1A,
	DESC_STRING,
	'D', 0, 'E', 0, 'A', 0, 'D', 0, 'C', 0, '0', 0, 'D', 0, 'E', 0, 'C', 0, 'A', 0, 'F', 0, 'E', 0,

// terminating zero
	0
};

/*************************************************************************
	HandleClassRequest
	==================
		Handle mass storage class request

**************************************************************************/
static int HandleClassRequest(TSetupPacket *pSetup, int *piLen, U8 **ppbData)
{
	if (pSetup->wIndex != 0) {
//		DBG("Invalid idx %X\n", pSetup->wIndex);
		return 0;
	}
	if (pSetup->wValue != 0) {
//		DBG("Invalid val %X\n", pSetup->wValue);
		return 0;
	}

	switch (pSetup->bRequest) {

	// get max LUN
	case 0xFE:
		*ppbData[0] = 0;		// No LUNs
		*piLen = 1;
		break;

	// MSC reset
	case 0xFF:
		if (pSetup->wLength > 0) {
			return 0;
		}
		MSCBotReset();
		break;

	default:
//		DBG("Unhandled class\n");
		return 0;
	}
	return 1;
}

void UsbSysInit(void)
{
	USBInit();
	USBHwNakIntEnable(INACK_BI);	// enable bulk-in interrupts on NAKs
	// these are required to get the BOT protocol going again after a STALL
	USBRegisterDescriptors(abDescriptors);	// register descriptors
	USBRegisterRequestHandler(REQTYPE_TYPE_CLASS, HandleClassRequest, abClassReqData);	// register class request handler
	// register endpoint handlers
	USBHwRegisterEPIntHandler(MSC_BULK_IN_EP, MSCBotBulkIn);
	USBHwRegisterEPIntHandler(MSC_BULK_OUT_EP, MSCBotBulkOut);
	USBHwConnect(1);	// connect to bus
}
