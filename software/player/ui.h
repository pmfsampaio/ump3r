/*
 * ui.h
 *
 *  Created on: 28 Feb 2011
 *      Author: psampaio
 */

#ifndef UI_H_
#define UI_H_

typedef struct
{	char itemType;
	char *itemText;
	void *itemCallback;
} MenuItemDsc;

#define MENU_ITEM_TYPE_MAIN_MENU_HEADER		0
#define MENU_ITEM_TYPE_SUB_MENU_HEADER		1
#define MENU_ITEM_TYPE_SUB_MENU				2
#define MENU_ITEM_TYPE_COMMAND				3
#define MENU_ITEM_TYPE_CALLBACK				4
#define MENU_ITEM_TYPE_END_OF_MENU			5

/*
 * Menu definitions
 */
#define UI_TITLE_BAR_HEIGHT					16
#define UI_TITLE_BAR_TOP 					0+9
#define UI_TITLE_BAR_BOTTOM 				(UI_TITLE_BAR_TOP + UI_TITLE_BAR_HEIGHT)
#define UI_TITLE_BAR_LEFT 					0
#define UI_TITLE_BAR_RIGHT 					129
#define UI_TITLE_BAR_CENTER_X				((UI_TITLE_BAR_RIGHT - UI_TITLE_BAR_LEFT) / 2)


#define UI_BOTTOM_BAR_HEIGHT				16
#define UI_BOTTOM_BAR_BOTTOM 				129
#define UI_BOTTOM_BAR_TOP 					(UI_BOTTOM_BAR_BOTTOM - (UI_BOTTOM_BAR_HEIGHT - 1))
#define UI_BOTTOM_BAR_LEFT 					0
#define UI_BOTTOM_BAR_RIGHT 				129
#define UI_BOTTOM_BAR_LEFT_BUTTON			29
#define UI_BOTTOM_BAR_RIGHT_BUTTON			99


#define UI_WORKSPACE_LEFT				0
#define UI_WORKSPACE_RIGHT			129
#define UI_WORKSPACE_WIDTH_WITH_BORDERS	((UI_WORKSPACE_RIGHT - UI_WORKSPACE_LEFT + 1) - 6)
#define UI_WORKSPACE_TOP 				UI_TITLE_BAR_BOTTOM
#define UI_WOKRSPACE_BOTTOM 			(UI_BOTTOM_BAR_TOP - 1)

#define UI_WORKSPACE_HEIGHT				(UI_WOKRSPACE_BOTTOM - UI_WORKSPACE_TOP)


#define UI_WORKSPACE_BACKGROUND			WHITE

#define UI_TITLE_BAR_FOREGROUND				WHITE
#define UI_TITLE_BAR_BACKGROUND_BB			BLUE4
#define UI_TITLE_BAR_BACKGROUND_B			BLUE2
#define UI_TITLE_BAR_BACKGROUND				BLUE2
#define UI_TITLE_BAR_BACKGROUND_D			BLUE1

#define UI_BOTTOM_BAR_BACKGROUND			BLUE6
#define UI_BOTTOM_BAR_BUTTON_BACKGROUND_B	BLUE4
#define UI_BOTTOM_BAR_BUTTON_BACKGROUND		BLUE3
#define UI_BOTTOM_BAR_BUTTON_FOREGROUND		WHITE

#define UI_ITEM_FOREGROUND					BLACK
#define UI_ITEM_BACKGROUND					UI_WORKSPACE_BACKGROUND
#define UI_SELECT_ITEM_FOREGROUND			WHITE
#define UI_SELECT_ITEM_BACKGROUND_1			BLUE2
#define UI_SELECT_ITEM_BACKGROUND_2			BLUE6
#define UI_SELECT_ITEM_BACKGROUND			BLUE5
#define UI_SELECT_ITEM_BACKGROUND_8			BLUE6
#define UI_SELECT_ITEM_BACKGROUND_9			BLACK

#define UI_SLIDER_BAR_BB					BLUE8
#define UI_SLIDER_BAR_B						BLUE6
#define UI_SLIDER_BAR						BLUE5

#define UI_SLIDER_BAR_BACKGROUND_BB			WHITE
#define UI_SLIDER_BAR_BACKGROUND_B			GREEN//GRAY6
#define UI_SLIDER_BAR_BACKGROUND	    	GREEN//GRAY5


#define MENU_ACTION_CALLBACK_GET_TEXT		0
#define MENU_ACTION_CALLBACK_SELECTION		1

void UISetEventHandler(void (*newEventHandler)());

void UISelectMenuDisplay(MenuItemDsc *menu);
void UIExecuteEvent(int eventId);
void UIBlankDisplaySpace(void);
void UIDrawTitleBar(char *text);

void UISelectParentMenu(void);
void UIDrawBottomBar(char *leftText, char *rightText);
void UISelectPreviousMenuDisplay(void);
int UIGetSelectItemIdx(void);

#define SLIDER_ACTION_CALLBACK_VALUE_CHANGED		1
#define SLIDER_ACTION_CALLBACK_VALUE_SET			2
#define SLIDER_ACTION_CALLBACK_CANCELED				3

void UISelectSliderDisplay(char *title, int minValue, int maxValue,
		int step, int stepAutoRepeat, int initialValue, char *text,
		int showValue, int showLimits, void(*callback)());


//
// Message Display types
//
#define MESSAGE_DISPLAY_BACK					1
#define MESSAGE_DISPLAY_OK_BACK					2
#define MESSAGE_DISPLAY_YES_NO					3

//
// Message Display callback actions
//
#define MESSAGE_ACTION_CALLBACK_BACK				1
#define MESSAGE_ACTION_CALLBACK_OK					2
#define MESSAGE_ACTION_CALLBACK_YES					3
#define MESSAGE_ACTION_CALLBACK_NO					4

void UISelectMessageDisplay(char *title, char *text, int justify,
		int type, void (*callback)());

/*
 *
 void TextMultipleLine(char *text, int x, int y, int lineSpacing, int justify,
		int maxLineWidth, int fColor, int bColor);
*/
void TextBrokenIntoMultipleLines(char *S, int X, int Y, int LineSpacing, int Justify,
		int MaxLineWidth, int Font, int ForegroundRGB8, int BackgroundRGB8);
#endif /* UI_H_ */
