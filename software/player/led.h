/*
 * led.h
 *
 *  Created on: 13 Mar 2011
 *      Author: psampaio
 */

#ifndef LED_H_
#define LED_H_

void LEDInit(void);
void LEDBlue(int state);
void LEDGreen(int state);
void LEDRed(int state);
void LEDAllOff();

#endif /* LED_H_ */
