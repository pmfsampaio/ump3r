/*
 * vic.c
 *
 *  Created on: 16 Feb 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "vic.h"

void __attribute__ ((interrupt("IRQ"))) VICDefaultIsr(void) {
	VICVectAddr = 0; /* Acknowledge Interrupt */
	while (1)
		;
}

void VICInit(void) {
	unsigned int i;
	unsigned int *vectAddr, *vectCntl;

	VICIntEnClr = 0xffffffff;
	VICVectAddr = 0;
	VICIntSelect = 0;

	for (i = 0; i < VIC_SIZE; i++) {
		vectAddr = (unsigned int *) (VIC_BASE_ADDR + VECT_ADDR_INDEX + i * 4);
		vectCntl = (unsigned int *) (VIC_BASE_ADDR + VECT_CNTL_INDEX + i * 4);
		*vectAddr = 0;
		*vectCntl = 0;
	}
	VICDefVectAddr = (unsigned int) VICDefaultIsr;
	return;
}

int VICSetIrq(unsigned int intNumber, void *isrAddr) {
	unsigned int i;
	unsigned int *vectAddr, *vectCntl;

	VICIntEnClr = 1 << intNumber; /* Disable Interrupt */

	for (i = 0; i < VIC_SIZE; i++) {
		vectAddr = (unsigned int *) (VIC_BASE_ADDR + VECT_ADDR_INDEX + i * 4);
		vectCntl = (unsigned int *) (VIC_BASE_ADDR + VECT_CNTL_INDEX + i * 4);
		if (*vectAddr == 0) {
			*vectAddr = (unsigned int)isrAddr; /* set interrupt vector */
			*vectCntl = (unsigned int)(IRQ_SLOT_EN | intNumber);
			break;
		}
	}
	if (i == VIC_SIZE) {
		return -1; /* fatal error, can't find empty vector slot */
	}
	VICIntEnable = 1 << intNumber;
	return 0;
}

int VICUnsetIrq(unsigned int intNumber) {
	unsigned int i;
	unsigned int *vectAddr, *vectCntl;

	VICIntEnClr = 1 << intNumber; /* Disable Interrupt */

	for (i = 0; i < VIC_SIZE; i++) {
		vectAddr = (unsigned int *) (VIC_BASE_ADDR + VECT_ADDR_INDEX + i * 4);
		vectCntl = (unsigned int *) (VIC_BASE_ADDR + VECT_CNTL_INDEX + i * 4);
		if ((*vectCntl & ~IRQ_SLOT_EN) == intNumber) {
			*vectAddr = 0;
			*vectCntl &= ~IRQ_SLOT_EN; /* disable SLOT_EN bit */
			break;
		}
	}
	if (i == VIC_SIZE) {
		return -1; /* fatal error, can't find interrupt number
		 in vector slot */
	}
	VICIntEnable = 1 << intNumber; /* Enable Interrupt */
	return 0;
}

static inline unsigned __get_cpsr(void) {
	unsigned long retval;
	asm volatile (" mrs  %0, cpsr" : "=r" (retval) : /* no inputs */);
	return retval;
}

static inline void __set_cpsr(unsigned val) {
	asm volatile (" msr  cpsr, %0" : /* no outputs */: "r" (val) );
}

unsigned int enableIRQ(void) {
	unsigned int _cpsr;

	_cpsr = __get_cpsr();
	__set_cpsr(_cpsr & ~I_Bit);
	return _cpsr;
}

unsigned int disableIRQ(void) {
	unsigned _cpsr;

	_cpsr = __get_cpsr();
	__set_cpsr(_cpsr | I_Bit);
	return _cpsr;
}

