/*
 * player.c
 *
 *  Created on: 14 Oct 2010
 *      Author: psampaio
 */
#include <stdio.h>
#include <string.h>
#include "lpc214x.h"
#include "ump3r.h"
//#include "vic.h"
#include "sysclk.h"
#include "lcd.h"
#include "rtc.h"
#include "ssp.h"
#include "ui.h"
#include "button.h"
#include "adc.h"
#include "led.h"
#include "vs1033.h"
#include "pff.h"
#include "integer.h"
#include "mp3info.h"
#include "usbsys.h"

extern const unsigned char Image_logo[];
extern const unsigned char Image_ump3r1[];

#define VOLUME_ICON_LEFT	( (LCD_COLUMNS/2) - 10)
#define VOLUME_ICON_TOP	3
#define VOLUME_ICON_RIGHT	(VOLUME_ICON_LEFT + 2)
#define VOLUME_ICON_BOTTOM	(VOLUME_ICON_TOP + 2)

#define VOLUME_MIN	0
#define VOLUME_MAX	16

#define VOLUME_BORDER_LEFT		( ( LCD_COLUMNS - (16*4) ) / 2 )
#define VOLUME_BORDER_TOP		( UI_WORKSPACE_TOP + 60 )
#define VOLUME_BORDER_RIGHT		( VOLUME_BORDER_LEFT + (16*4) + 2 )
#define VOLUME_BORDER_BOTTOM	( VOLUME_BORDER_TOP + 5 )

#define MUSICTIME_BORDER_LEFT		( ( LCD_COLUMNS - 100 ) / 2 )
#define MUSICTIME_BORDER_TOP		( UI_WORKSPACE_TOP + 80 )
#define MUSICTIME_BORDER_RIGHT		( MUSICTIME_BORDER_LEFT + 100 + 2 )
#define MUSICTIME_BORDER_BOTTOM		( MUSICTIME_BORDER_TOP + 5 )

char volumeTable[] = { 255, 240, 224, 208, 140, 120, 100, 90, 80, 70, 60, 50,
		40, 30, 20, 10, 0 };

#define DEBUG(_v)  {_v}

int Stop(int i) {
	volatile int dummy;
	dummy++;
	for (;;);
	return 0;
}

typedef enum {
	APPLICATION_READ_FILES,
	APPLICATION_IDLE,
	APPLICATION_POWER_DOWN,
	APPLICATION_BUSY
} ApplicationStates;

static ApplicationStates _appState;

static int contrastValue = 0x29;
static int volume = VOLUME_MAX - 3;
static int loudness;
static int playing;
static int currentMusic;
static int maxPlayList;
static int musicTime;
unsigned int idleTime;

#define POWER_DOWN_TIMEOUT	_MS(10000)
#define MAX_FILES			100

typedef struct {
	char filename[14];
	char title[31];
	char artist[31];
	int time;
} PlayListDsc;

static PlayListDsc playList[MAX_FILES];
//FILINFO filePlaying;

DIR dir;
FATFS fatfs;

void InitializeBeat(void) {
	char str[] = "Initialising...";
	static int i = 0;

	str[strlen(str) - i] = 0;
	LCDPutStr(
			str,
			(UI_WORKSPACE_WIDTH_WITH_BORDERS - strlen("Initialising...") * 8)
					/ 2, UI_WORKSPACE_HEIGHT - 5, MEDIUM,
			UI_WORKSPACE_BACKGROUND, BLACK);
	str[strlen(str) - i] = '.';
	i = (i + 1) % 3;
}

/*
 * VBat_min = 524 (off)
 * VBat_max = 6xx (full charge)
 */
int GetBatteryStatus(void) {
	int batValue;

	batValue = ADCRead(4);
	batValue -= 525;
	if (batValue < 0) batValue = 0;
	batValue /= 25;
	if (batValue > 4)
		batValue = 4;
	return batValue;
}

void UpdateBateryStatus(void)
{
	int batteryStatus;

	batteryStatus = GetBatteryStatus();
	LCDFilledRectangle(LCD_COLUMNS - 25, 1, LCD_COLUMNS - 11, 1, WHITE);
	LCDFilledRectangle(LCD_COLUMNS - 25, 7, LCD_COLUMNS - 11, 7, WHITE);
	LCDFilledRectangle(LCD_COLUMNS - 25, 1, LCD_COLUMNS - 25, 7, WHITE);
	LCDFilledRectangle(LCD_COLUMNS - 11, 1, LCD_COLUMNS - 11, 7, WHITE);
	LCDFilledRectangle(LCD_COLUMNS - 11, 3, LCD_COLUMNS - 8, 5, WHITE);
	if (batteryStatus == 0) {
		LCDFilledRectangle(LCD_COLUMNS - 24, 2, (LCD_COLUMNS - 24) + 12, 6,
				RED);
	} else {
		LCDFilledRectangle(LCD_COLUMNS - 24, 2,
				(LCD_COLUMNS - 24) + 3 * batteryStatus, 6, GREEN);
	}
}

void UpdateStatusBar() {
	char clkStr[11];
	struct tm clk;

	if (RTCUpdated()) {
		RTCGetTime(&clk);
		sprintf(clkStr, "%02d:%02d:%02d", clk.tm_hour, clk.tm_min, clk.tm_sec);
		LCDPutStr(clkStr, 0, 0, SMALL, WHITE, BLACK);
		UpdateBateryStatus();
	}
}

MenuItemDsc configMenu[];
MenuItemDsc settingsMenu[];
MenuItemDsc playListMenu[8];

void UsbApplication(void);

static void StopEventHandler(int event) {

//	DEBUG(if ((_appState != APPLICATION_IDLE) && (_appState != APPLICATION_POWER_DOWN)) Stop(2););

	if (event && (_appState == APPLICATION_POWER_DOWN)) {
		idleTime = GetElapsedTicks(0);
		LCDBackLight(50);
	}
	switch (event) {
	case EVENT_USB:
		UsbApplication();
		break;
	case EVENT_UP_BUTTON_PUSHED:
		break;
	case EVENT_DOWN_BUTTON_PUSHED:
		break;
	case EVENT_LEFT_BUTTON_PUSHED:
		UISelectMenuDisplay(configMenu);
		break;
	case EVENT_CENTER_BUTTON_PUSHED:
	case EVENT_RIGHT_BUTTON_PUSHED:
		UISelectMenuDisplay(playListMenu);
		break;
	default:
		if (GetElapsedTicks(idleTime) > POWER_DOWN_TIMEOUT) {
			_appState = APPLICATION_POWER_DOWN;
			LCDBackLight(10);
		}
		break;
	}
}

static void StopApplication(void);

void PrepareVolumeIcon(void) {
	LCDFilledRectangle(VOLUME_ICON_LEFT, VOLUME_ICON_TOP, VOLUME_ICON_RIGHT,
			VOLUME_ICON_BOTTOM, WHITE);
	LCDFilledRectangle(VOLUME_ICON_RIGHT + 1, VOLUME_ICON_TOP - 1,
			VOLUME_ICON_RIGHT + 1, VOLUME_ICON_BOTTOM + 1, WHITE);
	LCDFilledRectangle(VOLUME_ICON_RIGHT + 2, VOLUME_ICON_TOP - 2,
			VOLUME_ICON_RIGHT + 2, VOLUME_ICON_BOTTOM + 2, WHITE);
}

void UpdateVolumeIcon(int vol) {
	LCDFilledRectangle(VOLUME_ICON_LEFT + 2 + 1 + 2,
			VOLUME_ICON_TOP - 1 - 1 - 1,
			VOLUME_ICON_LEFT + 2 + 1 + 2 + 1 + 1 + 3 + 3,
			VOLUME_ICON_TOP + 4 + 1 + 1 - 1, BLACK);
	if ((vol / 4) >= 1) {
		LCDFilledRectangle(VOLUME_ICON_RIGHT + 4, VOLUME_ICON_TOP - 1,
				VOLUME_ICON_RIGHT + 4, VOLUME_ICON_TOP - 1, GREEN);
		LCDFilledRectangle(VOLUME_ICON_RIGHT + 5, VOLUME_ICON_TOP,
				VOLUME_ICON_RIGHT + 5, VOLUME_ICON_BOTTOM, GREEN);
		LCDFilledRectangle(VOLUME_ICON_RIGHT + 4, VOLUME_ICON_BOTTOM + 1,
				VOLUME_ICON_RIGHT + 4, VOLUME_ICON_BOTTOM + 1, GREEN);

		if ((vol / 4) >= 2) {
			LCDFilledRectangle(VOLUME_ICON_RIGHT + 4 + 3,
					VOLUME_ICON_TOP - 1 - 1, VOLUME_ICON_RIGHT + 4 + 3,
					VOLUME_ICON_TOP - 1 - 1, GREEN);
			LCDFilledRectangle(VOLUME_ICON_RIGHT + 5 + 3, VOLUME_ICON_TOP - 1,
					VOLUME_ICON_RIGHT + 5 + 3, VOLUME_ICON_BOTTOM + 1, GREEN);
			LCDFilledRectangle(VOLUME_ICON_RIGHT + 4 + 3,
					VOLUME_ICON_BOTTOM + 1 + 1, VOLUME_ICON_RIGHT + 4 + 3,
					VOLUME_ICON_BOTTOM + 1 + 1, GREEN);

			if ((vol / 4) >= 3) {
				LCDFilledRectangle(VOLUME_ICON_RIGHT + 4 + 6,
						VOLUME_ICON_TOP - 1 - 2, VOLUME_ICON_RIGHT + 4 + 6,
						VOLUME_ICON_TOP - 1 - 2, GREEN);
				LCDFilledRectangle(VOLUME_ICON_RIGHT + 5 + 6,
						VOLUME_ICON_TOP - 2, VOLUME_ICON_RIGHT + 5 + 6,
						VOLUME_ICON_BOTTOM + 2, GREEN);
				LCDFilledRectangle(VOLUME_ICON_RIGHT + 4 + 6,
						VOLUME_ICON_BOTTOM + 1 + 2, VOLUME_ICON_RIGHT + 4 + 6,
						VOLUME_ICON_BOTTOM + 1 + 2, GREEN);
			}
		}
	}
}

void UpdateVolume(int vol) {
	LCDFilledRectangle(VOLUME_BORDER_LEFT + 1, VOLUME_BORDER_TOP + 1,
			(VOLUME_BORDER_LEFT + 1) + vol * 4, VOLUME_BORDER_BOTTOM - 1,
			BLUE3);
	LCDFilledRectangle(1 + (VOLUME_BORDER_LEFT + 1) + vol * 4,
			VOLUME_BORDER_TOP + 1, VOLUME_BORDER_RIGHT - 1,
			VOLUME_BORDER_BOTTOM - 1, WHITE);
	UpdateVolumeIcon(vol);
}

void PrepareVolume(void) {
	LCDFilledRectangle(VOLUME_BORDER_LEFT, VOLUME_BORDER_TOP,
			VOLUME_BORDER_LEFT, VOLUME_BORDER_BOTTOM, BLACK);
	LCDFilledRectangle(VOLUME_BORDER_RIGHT, VOLUME_BORDER_TOP,
			VOLUME_BORDER_RIGHT, VOLUME_BORDER_BOTTOM, BLACK);
	LCDFilledRectangle(VOLUME_BORDER_LEFT, VOLUME_BORDER_TOP,
			VOLUME_BORDER_RIGHT, VOLUME_BORDER_TOP, BLACK);
	LCDFilledRectangle(VOLUME_BORDER_LEFT, VOLUME_BORDER_BOTTOM,
			VOLUME_BORDER_RIGHT, VOLUME_BORDER_BOTTOM, BLACK);
}

#define PLAYLIST_SELECT_TITLE(_m)	((playList[_m].title[0] == 0) ? playList[_m].filename : playList[_m].title)

int BuildPlayList(void) {
	DIR dir;
	FATFS fatfs;
	FILINFO finfo;
	Mp3InfoDsc info;
	int res, i;

	res = pf_mount(&fatfs);
	res = pf_opendir(&dir, "");
	if (res != FR_OK)
		return 0;
	for (i = 0; i < MAX_FILES; i++) {
		res = pf_readdir(&dir, &finfo);
		if ((res != FR_OK) || !finfo.fname[0])
			break;
		memset(&info, 0, sizeof(Mp3InfoDsc));
		res = pf_open(/*&info.file, */finfo.fname);//, FA_READ);
		if (res != FR_OK)
			break;
		info.datasize = finfo.fsize;
		Mp3Info(&info, SCAN_QUICK, 0);
		strcpy(playList[i].filename, finfo.fname);
		strcpy(playList[i].title, info.id3.title);
		strcpy(playList[i].artist, info.id3.artist);
		playList[i].time = info.seconds;
		//f_close(&info.file);
		InitializeBeat();
	}
	return i;
}


void PrepareMusicTime(void) {
	LCDFilledRectangle(MUSICTIME_BORDER_LEFT, MUSICTIME_BORDER_TOP,
			MUSICTIME_BORDER_LEFT, MUSICTIME_BORDER_BOTTOM, BLACK);
	LCDFilledRectangle(MUSICTIME_BORDER_RIGHT, MUSICTIME_BORDER_TOP,
			MUSICTIME_BORDER_RIGHT, MUSICTIME_BORDER_BOTTOM, BLACK);
	LCDFilledRectangle(MUSICTIME_BORDER_LEFT, MUSICTIME_BORDER_TOP,
			MUSICTIME_BORDER_RIGHT, MUSICTIME_BORDER_TOP, BLACK);
	LCDFilledRectangle(MUSICTIME_BORDER_LEFT, MUSICTIME_BORDER_BOTTOM,
			MUSICTIME_BORDER_RIGHT, MUSICTIME_BORDER_BOTTOM, BLACK);
	LCDFilledRectangle(MUSICTIME_BORDER_LEFT + 1, MUSICTIME_BORDER_TOP + 1,
			MUSICTIME_BORDER_RIGHT - 1, MUSICTIME_BORDER_BOTTOM - 1, RED);
}

void UpdateMusicTime(void) {
	int width;
	static int lastWidth = 0;

	width = GetSeconds();
	if (width != lastWidth) {
		width = (width * 100 ) / musicTime;
	LCDFilledRectangle(MUSICTIME_BORDER_LEFT + 1, MUSICTIME_BORDER_TOP + 1,
			(MUSICTIME_BORDER_LEFT + 1) + width, MUSICTIME_BORDER_BOTTOM - 1,
			GREEN);
	lastWidth = width;
	}
}

mp3_playStatus_t PlayFile(void *file) {
	unsigned char buff[512];
	unsigned int read;

	pf_read(/*file, */buff, 512, &read);
	VS1033PlayEx(buff, read);
	if (read != 512)
		return PLAYSTATUS_ENDOFFILE;
	return ((IOPIN0 & MP3_DREQ) == 0) ? PLAYSTATUS_BUSY : PLAYSTATUS_OK;
}

static void PlayEventHandler(int event) {
	int i;

//	DEBUG(if (_appState != APPLICATION_BUSY) Stop(1););

	switch (event) {
	case EVENT_UP_BUTTON_PUSHED:
		//	case EVENT_UP_BUTTON_REPEAT:
		if (volume < VOLUME_MAX) {
			volume++;
			VS1033Volume(volumeTable[volume]);
			UpdateVolume(volume);
		}
		break;
	case EVENT_DOWN_BUTTON_PUSHED:
		//	case EVENT_DOWN_BUTTON_REPEAT:
		if (volume > VOLUME_MIN) {
			volume--;
			VS1033Volume(volumeTable[volume]);
			UpdateVolume(volume);
		}
		break;
	case EVENT_CENTER_BUTTON_PUSHED:
	case EVENT_LEFT_BUTTON_PUSHED:
		if (playing)
			//f_close(&filePlaying);
		StopApplication();
		return;
		break;
	case EVENT_RIGHT_BUTTON_PUSHED:
		UISelectMenuDisplay(configMenu);
		return;
		break;
	default:
		break;
	}
	if (playing) {
		i = (8*128) / 32;
		while (i--) {
			if (PlayFile(NULL/*&filePlaying*/) == PLAYSTATUS_ENDOFFILE) {
				//f_close(&filePlaying);
				playing = 0;
				StopApplication();
				return; //break;
			}
		}
		UpdateMusicTime();
	}
}

static void UsbEventHandler(int event) {
	USBHwISR();
	if ((IOPIN0 & (1 << 23)) == 0) {
		maxPlayList = 0;
		StopApplication();
	}
}

void UsbApplication(void) {
	LCDClear(BLACK);
	LCDBitmap(Image_logo, 4, UI_TITLE_BAR_HEIGHT);
	LCDPutStr("Connected",
			(UI_WORKSPACE_WIDTH_WITH_BORDERS - strlen("Connected") * 8) / 2,
			UI_WORKSPACE_HEIGHT - 5, MEDIUM, UI_WORKSPACE_BACKGROUND, BLACK);
	UIDrawBottomBar(0, 0);
	UISetEventHandler(UsbEventHandler);
}

void PlayApplication(void) {
	int res;
	char music[30 + 30 + 5 + 1];

//	DEBUG(if (playing != 0) Stop(3););

	if (playing == 0) {
		currentMusic = UIGetSelectItemIdx() - 1;
		res = pf_mount(&fatfs);
		res = pf_opendir(&dir, "");
		res = pf_open(/*&filePlaying,*/ playList[currentMusic].filename);//, FA_READ);
		if (res == FR_OK) {
			playing = 1;
			musicTime = playList[currentMusic].time;
			VS1033Volume(volumeTable[volume]);
			SetSecond(playList[currentMusic].time);
		}
	}
	if (playing == 1) {
		LCDFilledRectangle(UI_WORKSPACE_LEFT, UI_TITLE_BAR_TOP,
				UI_WORKSPACE_RIGHT, UI_WOKRSPACE_BOTTOM,
				UI_WORKSPACE_BACKGROUND);
		if (playList[currentMusic].time) {
			sprintf(music, "%s\n%s\n%02d:%02d",
					PLAYLIST_SELECT_TITLE(currentMusic),
					playList[currentMusic].artist,
					playList[currentMusic].time / 60,
					playList[currentMusic].time % 60);
		} else {
			strcpy(music, playList[currentMusic].filename);
		}
		TextBrokenIntoMultipleLines(music, UI_WORKSPACE_WIDTH_WITH_BORDERS / 2,
				UI_WORKSPACE_TOP + 2, 10, JUSTIFY_CENTER, 100, SMALL, BLUE,
				UI_WORKSPACE_BACKGROUND);
		PrepareVolume();
		UpdateVolume(volume);
		LEDGreen(1);
		PrepareMusicTime();
		_appState = APPLICATION_BUSY;
	}
	configMenu[0].itemCallback = PlayApplication;
	UIDrawBottomBar("Stop", "Menu");
	UISetEventHandler(PlayEventHandler);
}

static void StopApplication(void) {
	int i;

	if (maxPlayList == 0) {
		maxPlayList = BuildPlayList();
		playListMenu[0].itemType = MENU_ITEM_TYPE_MAIN_MENU_HEADER;
		playListMenu[0].itemText = "Play List";
		playListMenu[0].itemCallback = StopApplication;
		for (i = 0; i < maxPlayList; i++) {
			playListMenu[i + 1].itemType = MENU_ITEM_TYPE_COMMAND;
			playListMenu[i + 1].itemText = PLAYLIST_SELECT_TITLE(i);
			playListMenu[i + 1].itemCallback = PlayApplication;
		}
		playListMenu[i + 1].itemType = MENU_ITEM_TYPE_END_OF_MENU;
		playListMenu[i + 1].itemText = "";
		playListMenu[i + 1].itemCallback = 0;
	}

	configMenu[0].itemCallback = StopApplication;
	VS1033Stop();
	VS1033Volume(0xff);
	playing = 0;
	LEDGreen(0);
	_appState = APPLICATION_IDLE;
	idleTime = GetElapsedTicks(0);

	LCDFilledRectangle(UI_WORKSPACE_LEFT, UI_TITLE_BAR_TOP, UI_WORKSPACE_RIGHT,
			UI_WOKRSPACE_BOTTOM, UI_WORKSPACE_BACKGROUND);
	LCDBitmap(Image_ump3r1, 0, UI_WORKSPACE_TOP + 2);
	PrepareVolumeIcon();
	UpdateVolumeIcon(volume);
	UIDrawBottomBar("Menu", "Select");
	UISetEventHandler(StopEventHandler);
}

static void SetContrastCallback(int action, int value) {
	switch (action) {
	case SLIDER_ACTION_CALLBACK_VALUE_CHANGED:
		LCDContrast(value);
		break;
	case SLIDER_ACTION_CALLBACK_VALUE_SET:
		LCDContrast(value);
		contrastValue = value;
		UISelectPreviousMenuDisplay();
		break;
	case SLIDER_ACTION_CALLBACK_CANCELED:
		LCDContrast(contrastValue);
		UISelectPreviousMenuDisplay();
		break;
	}
}

static void SetContrast(void) {
	UISelectSliderDisplay("Settings", 0, 63, 2, 2, contrastValue,
			"Set contrast", 1, 1, SetContrastCallback);
}

char *LoudnessCallback(int action) {
	switch (action) {
	case MENU_ACTION_CALLBACK_SELECTION:
		loudness = (loudness) ? 0 : 1;
		break;
	case MENU_ACTION_CALLBACK_GET_TEXT:
		switch (loudness) {
		case 0:
			return ("ON");
		case 1:
			return ("OFF");
		}
		break;
	default:
		break;
	}
	return 0;
}

int hour, min, sec;

char *HourCallback(int action) {
	switch (action) {
	case MENU_ACTION_CALLBACK_SELECTION:
		hour++;
		if (hour == 23)
			hour = 0;
		break;
	case MENU_ACTION_CALLBACK_GET_TEXT:
		switch (hour) {
		case 0:
			return ("0");
		case 1:
			return ("1");
		}
		break;
	default:
		break;
	}
	return 0;
}

char *MinutesCallback(int action) {
	switch (action) {
	case MENU_ACTION_CALLBACK_SELECTION:
		min++;
		if (min == 59)
			min = 0;
		break;
	case MENU_ACTION_CALLBACK_GET_TEXT:
		switch (min) {
		case 0:
			return ("0");
		case 1:
			return ("1");
		}
		break;
	default:
		break;
	}
	return 0;
}

char *SecondsCallback(int action) {
	switch (action) {
	case MENU_ACTION_CALLBACK_SELECTION:
		sec++;
		if (sec == 59)
			sec = 0;
		break;
	case MENU_ACTION_CALLBACK_GET_TEXT:
		switch (sec) {
		case 0:
			return ("0");
		case 1:
			return ("1");
		}
		break;
	default:
		break;
	}
	return 0;
}

static void ShowAboutCallback(int Action) {
	switch (Action) {
	case MESSAGE_ACTION_CALLBACK_BACK:
		UISelectPreviousMenuDisplay();
		break;
	}
}

static void ShowAbout(void) {
	UISelectMessageDisplay("About",
			"Ultimate MP3 Reader\nV1.0\n\n(c) 2011 P. Sampaio", JUSTIFY_CENTER,
			MESSAGE_DISPLAY_BACK, ShowAboutCallback);
}

MenuItemDsc SetClockMenu[] = { { MENU_ITEM_TYPE_SUB_MENU_HEADER,
		"Settings Clock", settingsMenu }, { MENU_ITEM_TYPE_CALLBACK, "Hours: ",
		HourCallback },
		{ MENU_ITEM_TYPE_CALLBACK, "Minutes: ", MinutesCallback }, {
				MENU_ITEM_TYPE_CALLBACK, "Seconds: ", SecondsCallback }, {
				MENU_ITEM_TYPE_END_OF_MENU, "", 0 } };

MenuItemDsc settingsMenu[] = { { MENU_ITEM_TYPE_SUB_MENU_HEADER, "Settings",
		configMenu },
		{ MENU_ITEM_TYPE_CALLBACK, "Loudness: ", LoudnessCallback }, {
				MENU_ITEM_TYPE_SUB_MENU, "Set clock ", SetClockMenu }, {
				MENU_ITEM_TYPE_COMMAND, "Set contrast", SetContrast }, {
				MENU_ITEM_TYPE_END_OF_MENU, "", 0 } };

MenuItemDsc configMenu[] = { { MENU_ITEM_TYPE_MAIN_MENU_HEADER, "Main menu",
		StopApplication },
		{ MENU_ITEM_TYPE_SUB_MENU, "Settings", settingsMenu }, {
				MENU_ITEM_TYPE_COMMAND, "About", ShowAbout }, {
				MENU_ITEM_TYPE_END_OF_MENU, "", 0 } };

int main(void) {
	int event;

	SCKInit();
	RTCInit();
	SSPInit();
	LCDInit();
	ADCInit();
	LEDInit();
	VS1033Init();
	UsbSysInit();

	_appState = APPLICATION_IDLE;

	LCDClear(BLACK);
	LCDBackLight(50);
	LCDContrast(contrastValue);
	VS1033Volume(0xFF);
	LCDBitmap(Image_logo, 4, UI_TITLE_BAR_HEIGHT);
	InitializeBeat();
	StopApplication();
	UpdateBateryStatus();
#if 1
	while (1) {
		event = GetButtonsEvents();
		UIExecuteEvent(event);
		UpdateStatusBar();
	}
#endif
	for(;;);
	return 0;
}
