/*
 * ssp.h
 *
 *  Created on: 25 Feb 2011
 *      Author: psampaio
 */

#ifndef SSP_H_
#define SSP_H_

#define SSP_CR0_DSS_4   ((unsigned int) 0x00000003)
#define SSP_CR0_DSS_5   ((unsigned int) 0x00000004)
#define SSP_CR0_DSS_6   ((unsigned int) 0x00000005)
#define SSP_CR0_DSS_7   ((unsigned int) 0x00000006)
#define SSP_CR0_DSS_8   ((unsigned int) 0x00000007)
#define SSP_CR0_DSS_9   ((unsigned int) 0x00000008)
#define SSP_CR0_DSS 10  ((unsigned int) 0x00000009)
#define SSP_CR0_DSS_11  ((unsigned int) 0x0000000a)
#define SSP_CR0_DSS_12  ((unsigned int) 0x0000000b)
#define SSP_CR0_DSS_13  ((unsigned int) 0x0000000c)
#define SSP_CR0_DSS_14  ((unsigned int) 0x0000000d)
#define SSP_CR0_DSS_15  ((unsigned int) 0x0000000e)
#define SSP_CR0_DSS_16  ((unsigned int) 0x0000000f)

#define SSP_CR0_FRF_SPI ((unsigned int) 0x00000000)
#define SSP_CR0_FRF_SSI ((unsigned int) 0x00000010)
#define SSP_CR0_FRF_MW  ((unsigned int) 0x00000020)

#define SSP_CR0_CPOL    ((unsigned int) 0x00000040)
#define SSP_CR0_CPHA    ((unsigned int) 0x00000080)
#define SSP_CR0_SCR     ((unsigned int) 0x0000ff00)

void SSPInit(void);
inline void SSPSetMode(int dataLen, int frameFormat, unsigned int frequency);
inline unsigned short SSPTransferByte(unsigned short c);
inline unsigned short SSPSendByte(unsigned short c);
inline void SSPWaitTxEmpty(void);
inline void SSPWaitReady(void);
void SSPSendBlock(const unsigned char *pData, unsigned int size);
void SSPReceiveBlock(unsigned char *pData, unsigned int size);

#endif /* SSP_H_ */
