/*
 * button.c
 *
 *  Created on: 27 Feb 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "ump3r.h"
#include "sysclk.h"
#include "led.h"
#include "button.h"

static BUTTON_STATE ButtonState[LAST_BUTTON_IDX + 1];

static int CheckButton(int);

//
// timer values for dealing with buttons
//
// 5 * 25.6us * 256 = ~38ms
#define BUTTON_DEBOUNCE_PERIOD 4

// 120 * 25.6us * 256 = ~786ms
#define BUTTON_AUTO_REPEAT_DELAY 79*100

// 20 * 25.6us * 256 = ~131ms
#define BUTTON_AUTO_REPEAT_RATE 13

int GetButtonsEvents(void)
{	int Event;

	// check USB
	if (IOPIN0 & (1 << 23)) return EVENT_USB;
	//
	// check each of the 4 buttons for an event
	//
	Event = CheckButton(LEFT_BUTTON_IDX);
	if (Event != EVENT_NONE)
		return(Event);

	Event = CheckButton(RIGHT_BUTTON_IDX);
	if (Event != EVENT_NONE)
		return(Event);

	Event = CheckButton(UP_BUTTON_IDX);
	if (Event != EVENT_NONE)
		return(Event);

	Event = CheckButton(DOWN_BUTTON_IDX);
	if (Event != EVENT_NONE)
		return(Event);

	Event = CheckButton(CENTER_BUTTON_IDX);
	if (Event != EVENT_NONE)
		return(Event);

	return(EVENT_NONE);
}



//
// check for an event from a push button
//		Enter:	ButtonIdx = index of which button to test
//		Exit:	event value returned, EVENT_NONE returned if no event
//
static int CheckButton(int ButtonIdx)
{
	int ButtonValue;
	int EventBase;
	int TimerHighByte;
	unsigned int CurrentTime;

	LEDBlue(0);
	//
	// read the IO bit attached to the selected button
	//
	switch(ButtonIdx)
	{	case LEFT_BUTTON_IDX:
			ButtonValue = IOPIN1 & JOYSTICK_LEFT;
			EventBase = EVENT_LEFT_BUTTON_BASE;
			break;
		case RIGHT_BUTTON_IDX:
			ButtonValue = IOPIN1 & JOYSTICK_RIGHT;
			EventBase = EVENT_RIGHT_BUTTON_BASE;
			break;
		case UP_BUTTON_IDX:
			ButtonValue = IOPIN1 & JOYSTICK_UP;
			EventBase = EVENT_UP_BUTTON_BASE;
			break;
		case DOWN_BUTTON_IDX:
			ButtonValue = IOPIN1 & JOYSTICK_DOWN;
			EventBase = EVENT_DOWN_BUTTON_BASE;
			break;
		case CENTER_BUTTON_IDX:
			ButtonValue = IOPIN0 & JOYSTICK_CENTER;
			EventBase = EVENT_CENTER_BUTTON_BASE;
			break;
		default:
			ButtonValue = 0;
			EventBase = 0;
			break;
	}

	if ((ButtonState[ButtonIdx].State == WAITING_FOR_BUTTON_DOWN) && (ButtonValue != 0))
		return(EVENT_NONE);							// return no new event

	TimerHighByte = CurrentTime = GetElapsedTicks(0);
	if (CurrentTime < ButtonState[ButtonIdx].EventStartTime)
		CurrentTime += 256;

	switch(ButtonState[ButtonIdx].State)
	{	case WAITING_FOR_BUTTON_DOWN:			// waiting for button to go down
			if (ButtonValue == 0)				// check if button is now down
			{									// button down, start timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				ButtonState[ButtonIdx].State = DEBOUNCE_AFTER_BUTTON_DOWN;
				LEDBlue(1);
				return(EventBase + BUTTON_PUSHED); // return button "pressed" event
			}
			break;

		case DEBOUNCE_AFTER_BUTTON_DOWN:		// waiting for timer after button has gone down
												// check if it has been up long enough
			if (CurrentTime >= ((unsigned int) ButtonState[ButtonIdx].EventStartTime + BUTTON_DEBOUNCE_PERIOD))
			{									// debouncing period over, start auto repeat timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				ButtonState[ButtonIdx].State = WAITING_FOR_BUTTON_UP;
			}
			break;

		case WAITING_FOR_BUTTON_UP:				// waiting for button to go back up
			if (ButtonValue != 0)				// check if button is now up
			{									// button up, start debounce timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				ButtonState[ButtonIdx].State = DEBOUNCE_AFTER_BUTTON_UP;
				return(EventBase + BUTTON_RELEASED); // return button "released" event
			}
												// button still down, check if time to auto repeat
			if (CurrentTime >= ((unsigned int) ButtonState[ButtonIdx].EventStartTime + BUTTON_AUTO_REPEAT_DELAY))
			{									// reset auto repeat timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				ButtonState[ButtonIdx].State = WAITING_FOR_BUTTON_UP_AFTER_AUTO_REPEAT;
				return(EventBase + BUTTON_REPEAT); // return button "auto repeat" event
			}
			break;

		case WAITING_FOR_BUTTON_UP_AFTER_AUTO_REPEAT:// waiting for button to go back up
			if (ButtonValue != 0)				// check if button is now up
			{									// button up, start debounce timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				ButtonState[ButtonIdx].State = DEBOUNCE_AFTER_BUTTON_UP;
				return(EventBase + BUTTON_RELEASED); // return button "down" event
			}
												// button still down, check if time to auto repeat
			if (CurrentTime >= ((unsigned int) ButtonState[ButtonIdx].EventStartTime + BUTTON_AUTO_REPEAT_RATE))
			{									// reset auto repeat timer
				ButtonState[ButtonIdx].EventStartTime = TimerHighByte;
				return(EventBase + BUTTON_REPEAT); // return button "auto repeat" event
			}
			break;

		case DEBOUNCE_AFTER_BUTTON_UP:			// waiting for timer after button has gone up
			if (CurrentTime >= ((unsigned int) ButtonState[ButtonIdx].EventStartTime + BUTTON_DEBOUNCE_PERIOD))
				ButtonState[ButtonIdx].State = WAITING_FOR_BUTTON_DOWN;
			break;
	}

	return(EVENT_NONE);								// return no new event
}
