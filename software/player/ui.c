/*
 * ui.c
 *
 *  Created on: 28 Feb 2011
 *      Author: psampaio
 */
#include <stdlib.h>
#include <string.h>
#include "lcd.h"
#include "sysclk.h"
#include "button.h"
#include "ui.h"

static void DrawMenuItem(int index, int offset);
static void DrawButtonOnBottomBar(char *text, int centerX);
char *itoa(int value, char * str, int radix);

void UISelectPreviousMenuDisplay(void);

int fontId = MEDIUM;

MenuItemDsc *currentMenuTable;
int currentItemIdx;

#define UI_MENU_TOP_LINE_Y			(UI_WORKSPACE_TOP)
#define UI_MENU_LINE_SPACING_Y		13
#define UI_MENU_LEFT_MARGIN			4
#define UI_MENU_TEXT_HEIGHT_Y		8

#define UI_MENU_TEXT_WIDTH			8*12

static void (*currentEventHandler)(int);

static int menuOffset;
static int itemCurrentPosition;
static int itemPositionStep;
static int itemScrollTime;
static int stTime;

#define UI_MAXIMUM_ITEM_DISPLAY	6

static void MenuChangeSelection(int newItemIdx) {
	int prevIdx;

	if ((newItemIdx >= 1) && (currentMenuTable[newItemIdx].itemType
			!= MENU_ITEM_TYPE_END_OF_MENU) && (newItemIdx != currentItemIdx)) {
		prevIdx = currentItemIdx;
		currentItemIdx = newItemIdx;
		if (currentItemIdx > menuOffset + UI_MAXIMUM_ITEM_DISPLAY) {
			menuOffset++;
			for (prevIdx = currentItemIdx - UI_MAXIMUM_ITEM_DISPLAY + 1; prevIdx
					< currentItemIdx; prevIdx++)
				DrawMenuItem(prevIdx, menuOffset);
		} else {
			if (currentItemIdx == menuOffset) {
				menuOffset--;
				for (prevIdx = currentItemIdx; prevIdx < currentItemIdx
						+ UI_MAXIMUM_ITEM_DISPLAY - 1; prevIdx++)
					DrawMenuItem(prevIdx, menuOffset);
			}
		}
		itemCurrentPosition = 0;
		itemPositionStep = 2;
		stTime = GetElapsedTicks(0);
		itemScrollTime = _MS(1200);
		DrawMenuItem(prevIdx, menuOffset);
		DrawMenuItem(currentItemIdx, menuOffset);

	}
}

static void MenuEventHandler(int event) {
	void (*command)(void);
	void (*callback)(int);
	MenuItemDsc *subMenuTable;

	switch (event) {
	case EVENT_UP_BUTTON_PUSHED:
		MenuChangeSelection(currentItemIdx - 1);
		break;
	case EVENT_DOWN_BUTTON_PUSHED:
		MenuChangeSelection(currentItemIdx + 1);
		break;
	case EVENT_CENTER_BUTTON_PUSHED:
	case EVENT_RIGHT_BUTTON_PUSHED:
		switch (currentMenuTable[currentItemIdx].itemType) {
		case MENU_ITEM_TYPE_COMMAND:
			command
					= (void(*)(void)) currentMenuTable[currentItemIdx].itemCallback;
			if (command)
				(*command)();
			break;
		case MENU_ITEM_TYPE_CALLBACK:
			callback
					= (void(*)(int)) currentMenuTable[currentItemIdx].itemCallback;
			if (callback) {
				(*callback)(MENU_ACTION_CALLBACK_SELECTION);
				DrawMenuItem(currentItemIdx, menuOffset);
			}
			break;
		case MENU_ITEM_TYPE_SUB_MENU:
			subMenuTable
					= (MenuItemDsc *) currentMenuTable[currentItemIdx].itemCallback;
			UISelectMenuDisplay(subMenuTable);
			break;
		}
		break;
	case EVENT_LEFT_BUTTON_PUSHED:
		switch (currentMenuTable[0].itemType) {
		case MENU_ITEM_TYPE_MAIN_MENU_HEADER:
			command = (void(*)(void)) currentMenuTable[0].itemCallback;
			if (command)
				(*command)();
			break;
		case MENU_ITEM_TYPE_SUB_MENU_HEADER:
			if (currentMenuTable[0].itemCallback)
				UISelectParentMenu();
			break;
		}
		break;
	default:
		if ((strlen(currentMenuTable[currentItemIdx].itemText) * 8)
				> UI_MENU_TEXT_WIDTH) {
			if (GetElapsedTicks(stTime) > itemScrollTime) {
				itemCurrentPosition += itemPositionStep;
				DrawMenuItem(currentItemIdx, menuOffset);
				if ((itemCurrentPosition == 0) || (strlen(currentMenuTable[currentItemIdx].itemText) * 8)
						- itemCurrentPosition * 2 <= UI_MENU_TEXT_WIDTH) {
					itemPositionStep = -itemPositionStep;//(itemPositionStep == 2) ? -2 : 2;
					itemScrollTime = _MS(1200);
				}
				else {
					itemScrollTime = _MS(600);
				}
				stTime = GetElapsedTicks(0);
			}
		}
		break;
	}
}

void HorzGradient(int *color, int left, int right, int top, int lines) {
	while (lines) {
		LCDFilledRectangle(left, top, right, top, *color);
		lines--;
		top++;
		color++;
	}
}

void UISetEventHandler(void(*newEventHandler)()) {
	currentEventHandler = newEventHandler;
}

int UIGetSelectItemIdx(void)
{
	return currentItemIdx;
}

static void DrawMenuItem(int index, int offset) {
	int colorAbove[3];
	int colorBelow[2];
	unsigned int fColor;
	unsigned int bColor;
	unsigned int y;
	unsigned int x;
	char *rightText;
	char *(*callback)(int);

	if (index != currentItemIdx) {
		fColor = UI_ITEM_FOREGROUND;
		bColor = UI_ITEM_BACKGROUND;
		colorAbove[0] = UI_ITEM_BACKGROUND;
		colorAbove[1] = UI_ITEM_BACKGROUND;
		colorAbove[2] = UI_ITEM_BACKGROUND;
		colorBelow[0] = UI_ITEM_BACKGROUND;
		colorBelow[1] = UI_ITEM_BACKGROUND;
	} else {
		fColor = UI_SELECT_ITEM_FOREGROUND;
		bColor = UI_SELECT_ITEM_BACKGROUND;
		colorAbove[0] = UI_SELECT_ITEM_BACKGROUND_1;
		colorAbove[1] = UI_SELECT_ITEM_BACKGROUND_2;
		colorAbove[2] = UI_SELECT_ITEM_BACKGROUND;
		colorBelow[0] = UI_SELECT_ITEM_BACKGROUND_8;
		colorBelow[1] = UI_SELECT_ITEM_BACKGROUND_9;
	}
	switch (currentMenuTable[index].itemType) {
	case MENU_ITEM_TYPE_SUB_MENU:
		rightText = ">";
		break;
	case MENU_ITEM_TYPE_CALLBACK:
		callback = (char* (*)(int)) currentMenuTable[index].itemCallback;
		if (callback)
			rightText = (*callback)(MENU_ACTION_CALLBACK_GET_TEXT);
		else
			rightText = "";
		break;
	default:
		rightText = 0;
		break;
	}
	y = UI_MENU_TOP_LINE_Y + (((index - offset) - 1) * UI_MENU_LINE_SPACING_Y);
	HorzGradient(colorAbove, UI_WORKSPACE_LEFT, UI_WORKSPACE_RIGHT, y, 3);
	y += 3;
	LCDFilledRectangle(UI_WORKSPACE_LEFT, y, UI_WORKSPACE_RIGHT, y
			+ UI_MENU_TEXT_HEIGHT_Y, bColor);
	LCDPutStrEx(currentMenuTable[index].itemText, UI_MENU_LEFT_MARGIN, y,
			itemCurrentPosition, UI_MENU_LEFT_MARGIN + UI_MENU_TEXT_WIDTH, fontId,
			fColor, bColor);
	//	LCDPutStr(currentMenuTable[index].itemText, UI_MENU_LEFT_MARGIN, y,
	//			fontId, fColor, bColor);
	if (rightText) {
		x = UI_WORKSPACE_RIGHT - UI_MENU_LEFT_MARGIN - ((strlen(rightText) * 8)
				- 1);
		LCDPutStr(rightText, x, y, fontId, fColor, bColor);
	}
	y += UI_MENU_TEXT_HEIGHT_Y;
	HorzGradient(colorBelow, UI_WORKSPACE_LEFT, UI_WORKSPACE_RIGHT, y, 2);
}

void UIDrawTitleBar(char *text) {
	int color[3];

	color[0] = UI_TITLE_BAR_BACKGROUND_BB;
	color[1] = UI_TITLE_BAR_BACKGROUND_B;
	color[2] = UI_TITLE_BAR_BACKGROUND;
	HorzGradient(color, UI_TITLE_BAR_LEFT, UI_TITLE_BAR_RIGHT,
			UI_TITLE_BAR_TOP, 3);
	LCDFilledRectangle(UI_TITLE_BAR_LEFT, UI_TITLE_BAR_TOP + 3,
			UI_TITLE_BAR_RIGHT, 8 + UI_TITLE_BAR_TOP + 3,
			UI_TITLE_BAR_BACKGROUND);
	LCDPutStr(text, UI_TITLE_BAR_CENTER_X - ((strlen(text) * 8) / 2),
			UI_TITLE_BAR_TOP + 3, fontId, UI_TITLE_BAR_FOREGROUND,
			UI_TITLE_BAR_BACKGROUND);
	color[0] = UI_TITLE_BAR_BACKGROUND;
	color[1] = UI_TITLE_BAR_BACKGROUND_D;
	HorzGradient(color, UI_TITLE_BAR_LEFT, UI_TITLE_BAR_RIGHT, 8
			+ UI_TITLE_BAR_TOP + 3, 2);
}

void SelectMenuDisplay(MenuItemDsc *menu, int item) {
	int menuIdx;
	int y;
	int offset = 0;

	currentMenuTable = menu;
	currentItemIdx = item;
	itemCurrentPosition = 0;

	UIDrawTitleBar(currentMenuTable[0].itemText);
	LCDFilledRectangle(UI_WORKSPACE_LEFT, UI_WORKSPACE_TOP, UI_WORKSPACE_RIGHT,
			UI_MENU_TOP_LINE_Y, UI_WORKSPACE_BACKGROUND);

	menuIdx = 1;
	while (currentMenuTable[menuIdx].itemType != MENU_ITEM_TYPE_END_OF_MENU) {
		DrawMenuItem(menuIdx, offset);
		menuIdx++;
		if (menuIdx > UI_MAXIMUM_ITEM_DISPLAY)
			break;
	}
	y = UI_MENU_TOP_LINE_Y + ((menuIdx - 1) * UI_MENU_LINE_SPACING_Y);
	LCDFilledRectangle(UI_WORKSPACE_LEFT, y, UI_WORKSPACE_RIGHT,
			UI_WOKRSPACE_BOTTOM, UI_WORKSPACE_BACKGROUND);
	UIDrawBottomBar((currentMenuTable[0].itemCallback) ? "Back" : "", "Select");
	UISetEventHandler(MenuEventHandler);
}

void UIDrawBottomBar(char *leftText, char *rightText) {
	LCDFilledRectangle(UI_BOTTOM_BAR_LEFT, UI_BOTTOM_BAR_TOP,
			UI_BOTTOM_BAR_RIGHT, UI_BOTTOM_BAR_BOTTOM, UI_BOTTOM_BAR_BACKGROUND);
	if (leftText) {
		DrawButtonOnBottomBar(leftText, UI_BOTTOM_BAR_LEFT_BUTTON);
	}
	if (rightText) {
		DrawButtonOnBottomBar(rightText, UI_BOTTOM_BAR_RIGHT_BUTTON);
	}
}

#define UI_BOTTOM_BAR_BUTTON_MIN_WIDTH		47

static void DrawButtonOnBottomBar(char *text, int centerX) {
	int colorTable[4];
	int textWidth;
	int textHeight;
	int textLeftX;
	int textRightX;
	int textTopY;
	int textBottomY;
	int extraWidth;
	int padOnLeft;
	int buttonLeftX;
	int buttonRightX;

	textWidth = strlen(text) * 8;
	textHeight = 8;
	textLeftX = centerX - textWidth / 2;
	textRightX = textLeftX + textWidth;
	textTopY = UI_BOTTOM_BAR_TOP + 4;
	textBottomY = textTopY + textHeight + 1;

	if (UI_BOTTOM_BAR_BUTTON_MIN_WIDTH > textWidth)
		extraWidth = UI_BOTTOM_BAR_BUTTON_MIN_WIDTH - textWidth;
	else
		extraWidth = 0;

	padOnLeft = extraWidth >> 1;
	buttonLeftX = textLeftX - padOnLeft;
	buttonRightX = textRightX + (extraWidth - padOnLeft);

	colorTable[0] = UI_BOTTOM_BAR_BACKGROUND;
	colorTable[1] = UI_BOTTOM_BAR_BACKGROUND;
	colorTable[2] = UI_BOTTOM_BAR_BUTTON_BACKGROUND_B;
	colorTable[3] = UI_BOTTOM_BAR_BUTTON_BACKGROUND;
	HorzGradient(colorTable, buttonLeftX, buttonRightX, UI_BOTTOM_BAR_TOP, 4);
	LCDFilledRectangle(buttonLeftX, textTopY, buttonRightX, textBottomY,
			UI_BOTTOM_BAR_BUTTON_BACKGROUND);

	LCDPutStr(text, textLeftX, textTopY, fontId,
			UI_BOTTOM_BAR_BUTTON_FOREGROUND, UI_BOTTOM_BAR_BUTTON_BACKGROUND);
	LCDSetPixel(UI_BOTTOM_BAR_BACKGROUND, buttonLeftX, UI_BOTTOM_BAR_TOP + 2);
	LCDSetPixel(UI_BOTTOM_BAR_BACKGROUND, buttonLeftX, textBottomY);
	LCDSetPixel(UI_BOTTOM_BAR_BACKGROUND, buttonRightX, UI_BOTTOM_BAR_TOP + 2);
	LCDSetPixel(UI_BOTTOM_BAR_BACKGROUND, buttonRightX, textBottomY);
}

#if 0
void TextMultipleLine(char *text, int x, int y, int lineSpacing, int justify,
		int maxLineWidth, int fColor, int bColor) {
	int xInitial = x;

	while (*text) {
		if (*text == '\n') {
			y += 10;
			x = xInitial;
		} else {
			LCDPutChar(*text, x, y, SMALL, fColor, bColor);
			x += 6;
			if (x > maxLineWidth) {
				y += 10;
				x -= maxLineWidth;
			}
		}
		text++;
	}
}

#endif

static void (*MessageDisplayCallbackFunc)(int);
static int messageDisplayType;

//
// constants used to draw the Slider Display
//
#define UI_MESSAGE_DISPLAY_CENTER_Y		((UI_WORKSPACE_TOP + UI_WOKRSPACE_BOTTOM) / 2)
#define UI_MESSAGE_DISPLAY_FOREGROUND	BLACK

static void MessageDisplayEventHandler(int event);

void UISelectMessageDisplay(char *title, char *text, int justify, int type,
		void(*callback)()) {
	int maxLineWidth = 0;
	int lineSpacing;
	int lineCount;
	int x, y;

	MessageDisplayCallbackFunc = callback;
	messageDisplayType = type;

	UIDrawTitleBar(title);
	UIBlankDisplaySpace();

	switch (justify) {
	case JUSTIFY_LEFT:
		x = 6;
		maxLineWidth = (UI_WORKSPACE_RIGHT - UI_WORKSPACE_LEFT + 1) - x - x;
		break;
	case JUSTIFY_CENTER:
		x = (UI_WORKSPACE_RIGHT + UI_WORKSPACE_LEFT) / 2;
		maxLineWidth = UI_WORKSPACE_WIDTH_WITH_BORDERS;
		break;
	case JUSTIFY_RIGHT:
		x = UI_WORKSPACE_RIGHT - 6;
		maxLineWidth = (UI_WORKSPACE_RIGHT - UI_WORKSPACE_LEFT + 1) - 12;
		break;
	}

	//
	// determine the starting Y coord for drawing the multi line label above the slider
	//
	lineSpacing = 8;//Font[FONT_TABLE_LINE_SPACING_IDX];
	lineCount = 2;//GetTextLineCountAfterBreakingText(Message, StringType, MaxLineWidth, Font);
	y = UI_MESSAGE_DISPLAY_CENTER_Y - 2 + (lineSpacing / 2) - (((lineCount - 1)
			* lineSpacing) / 2);
	TextBrokenIntoMultipleLines(text, UI_WORKSPACE_WIDTH_WITH_BORDERS/2, y, 10, JUSTIFY_CENTER, maxLineWidth, SMALL,
			UI_MESSAGE_DISPLAY_FOREGROUND, UI_WORKSPACE_BACKGROUND);

	//
	// draw the message text centered in the Display Space
	//
	//	LCDPutStr(Message, X, Y, SMALL, UI_MESSAGE_DISPLAY_FOREGROUND, UI_DISPLAY_SPACE_BACKGROUND);
	//	DrawTextBrokenIntoMultipleLines(Message, StringType,
	//			X, Y, LineSpacing, Justify, MaxLineWidth, Font,
	//			UI_MESSAGE_DISPLAY_FOREGROUND_RGB8, UI_DISPLAY_SPACE_BACKGROUND_RGB8);

	//
	// draw the ButtonBar and set the event handler
	//
	switch (messageDisplayType) {
	case MESSAGE_DISPLAY_OK_BACK:
		UIDrawBottomBar("Cancel", "OK");
		break;

	case MESSAGE_DISPLAY_YES_NO:
		UIDrawBottomBar("No", "Yes");
		break;

	default:
		UIDrawBottomBar("Back", "");
		break;
	}

	//
	// set the event handler
	//
	UISetEventHandler(MessageDisplayEventHandler);
}

//
// event handler for the Message Display
//
static void MessageDisplayEventHandler(int event) {
	int LeftButtonAction;
	int RightButtonAction;

	//
	// determine the possible actions to send the callback func based on the MessageDisplay type
	//
	switch (messageDisplayType) {
	case MESSAGE_DISPLAY_OK_BACK:
		LeftButtonAction = MESSAGE_ACTION_CALLBACK_OK;
		RightButtonAction = MESSAGE_ACTION_CALLBACK_BACK;
		break;

	case MESSAGE_DISPLAY_YES_NO:
		LeftButtonAction = MESSAGE_ACTION_CALLBACK_YES;
		RightButtonAction = MESSAGE_ACTION_CALLBACK_NO;
		break;

	default:
		LeftButtonAction = MESSAGE_ACTION_CALLBACK_BACK;
		RightButtonAction = MESSAGE_ACTION_CALLBACK_BACK;
		break;
	}

	//
	// send the action to the callback based on the button pressed
	//
	switch (event) {
	case EVENT_LEFT_BUTTON_PUSHED:
		(*MessageDisplayCallbackFunc)(LeftButtonAction);
		break;

	case EVENT_RIGHT_BUTTON_PUSHED:
		(*MessageDisplayCallbackFunc)(RightButtonAction);
		break;

	default:
		return;
	}
}

void UISelectMenuDisplay(MenuItemDsc *menu) {
	SelectMenuDisplay(menu, 1);
}

void UISelectPreviousMenuDisplay(void) {
	SelectMenuDisplay(currentMenuTable, currentItemIdx);
}

void UISelectParentMenu(void) {
	MenuItemDsc *menu;

	menu = (MenuItemDsc *) currentMenuTable[0].itemCallback;
	if (menu)
		SelectMenuDisplay(menu, 1);
}

void UIExecuteEvent(int eventId) {
	(*currentEventHandler)(eventId);
}

void UIBlankDisplaySpace(void) {
	LCDFilledRectangle(UI_WORKSPACE_LEFT, UI_WORKSPACE_TOP, UI_WORKSPACE_RIGHT,
			UI_WOKRSPACE_BOTTOM, UI_WORKSPACE_BACKGROUND);
}

//
// constants used to draw the Slider Display
//
#define UI_SLIDER_HEIGHT 				14
#define UI_SLIDER_WIDTH 				110
#define UI_SLIDER_PERIMETER_HEIGHT 		(UI_SLIDER_HEIGHT + 2)
#define UI_SLIDER_PERIMETER_WIDTH 		(UI_SLIDER_WIDTH + 2)

#define UI_SLIDER_CENTER_X				(((UI_WORKSPACE_LEFT + UI_WORKSPACE_RIGHT) / 2) + 1)
#define UI_SLIDER_TOP_Y					74

#define UI_SLIDER_LABEL_CENTER_Y		(UI_WORKSPACE_TOP + 46)
#define UI_SLIDER_LABEL_FOREGROUND		BLACK

#define UI_SLIDER_MINMAX_VALUES_Y		(UI_SLIDER_TOP_Y + UI_SLIDER_PERIMETER_HEIGHT + 3)

#define UI_SLIDER_NUMERIC_VALUE_Y		(UI_SLIDER_TOP_Y + UI_SLIDER_PERIMETER_HEIGHT + 6)

#define UI_SLIDER_PERIMETER				BLACK // GRAY
static int currentSliderMinValue;
static int currentSliderMaxValue;
static int currentSliderStep;
static int currentSliderStepAutoRepeat;
static int currentSliderValue;
static int currentSliderShowValue;

static void (*currentSliderCallback)(int, int);

static void SliderDrawValue(void);
static void SliderEventHandler(int event);

void UISelectSliderDisplay(char *title, int minValue, int maxValue, int step,
		int stepAutoRepeat, int initialValue, char *text, int showValue,
		int showLimits, void(*callback)()) {
	int borderLeft;
	int borderRight;
	int borderTop;
	int borderBottom;
	int lineWidth;
	char str[10];
	int y;

	currentSliderMinValue = minValue;
	currentSliderMaxValue = maxValue;
	currentSliderStep = step;
	currentSliderStepAutoRepeat = stepAutoRepeat;
	currentSliderShowValue = showValue;
	currentSliderCallback = callback;

	UIDrawTitleBar(title);
	UIBlankDisplaySpace();

	lineWidth = UI_WORKSPACE_WIDTH_WITH_BORDERS;
	y = UI_SLIDER_LABEL_CENTER_Y - (8/2);
	LCDPutStr(text, UI_SLIDER_CENTER_X - ((strlen(text)*8) / 2), y, fontId,
			UI_SLIDER_LABEL_FOREGROUND, UI_WORKSPACE_BACKGROUND);
	borderLeft = UI_SLIDER_CENTER_X - (UI_SLIDER_PERIMETER_WIDTH / 2);
	borderRight = borderLeft + UI_SLIDER_PERIMETER_WIDTH - 1;
	borderTop = UI_SLIDER_TOP_Y;
	borderBottom = UI_SLIDER_TOP_Y + UI_SLIDER_PERIMETER_HEIGHT - 1;
	LCDFilledRectangle(borderLeft, borderTop, borderRight, borderTop,
			UI_SLIDER_PERIMETER);
	LCDFilledRectangle(borderLeft, borderBottom, borderRight, borderBottom,
			UI_SLIDER_PERIMETER);
	LCDFilledRectangle(borderLeft, borderTop, borderLeft, borderBottom,
			UI_SLIDER_PERIMETER);
	LCDFilledRectangle(borderRight, borderTop, borderRight, borderBottom,
			UI_SLIDER_PERIMETER);
	if (showLimits) {
		itoa(minValue, str, 10);
		LCDPutStr(str, borderLeft - (strlen(str)*6/2), UI_SLIDER_MINMAX_VALUES_Y,
				SMALL, UI_SLIDER_LABEL_FOREGROUND,
				UI_WORKSPACE_BACKGROUND);
		itoa(maxValue, str, 10);
		LCDPutStr(str, borderRight - (strlen(str)*6/2), UI_SLIDER_MINMAX_VALUES_Y,
				SMALL, UI_SLIDER_LABEL_FOREGROUND,
				UI_WORKSPACE_BACKGROUND);
	}
	currentSliderValue = initialValue;
	SliderDrawValue();

	UIDrawBottomBar("Cancel", "Set");
	UISetEventHandler(SliderEventHandler);
}

static void SliderEventHandler(int event) {
	int newValue;

	switch (event) {
	case EVENT_UP_BUTTON_PUSHED:
	case EVENT_UP_BUTTON_REPEAT:
		if (event == EVENT_UP_BUTTON_PUSHED)
			newValue = currentSliderValue + currentSliderStep;
		else
			newValue = currentSliderValue + currentSliderStepAutoRepeat;
		if (newValue > currentSliderMaxValue)
			newValue = currentSliderMaxValue;
		if (newValue != currentSliderValue) {
			currentSliderValue = newValue;
			SliderDrawValue();
			(*currentSliderCallback)(SLIDER_ACTION_CALLBACK_VALUE_CHANGED,
					newValue);
		}
		break;
	case EVENT_DOWN_BUTTON_PUSHED:
	case EVENT_DOWN_BUTTON_REPEAT:
		if (event == EVENT_DOWN_BUTTON_PUSHED)
			newValue = currentSliderValue - currentSliderStep;
		else
			newValue = currentSliderValue - currentSliderStepAutoRepeat;
		if (newValue < currentSliderMinValue)
			newValue = currentSliderMinValue;
		if (newValue != currentSliderValue) {
			currentSliderValue = newValue;
			SliderDrawValue();
			(*currentSliderCallback)(SLIDER_ACTION_CALLBACK_VALUE_CHANGED,
					newValue);
		}
		break;
	case EVENT_RIGHT_BUTTON_PUSHED:
		(*currentSliderCallback)(SLIDER_ACTION_CALLBACK_VALUE_SET,
				currentSliderValue);
		break;
	case EVENT_LEFT_BUTTON_PUSHED:
		(*currentSliderCallback)(SLIDER_ACTION_CALLBACK_CANCELED,
				currentSliderValue);
		break;
	}
}

static void SliderDrawValue(void) {
	int colorTable[14];
	int left;
	int right;
	int top;
	int bottom;
	int barLenght;
	int blankLeft;
	char str[20];

	if (currentSliderValue <= currentSliderMinValue)
		barLenght = 0;
	else if (currentSliderValue >= currentSliderMaxValue)
		barLenght = UI_SLIDER_WIDTH - 1;
	else
		barLenght = ((long) (currentSliderValue - currentSliderMinValue)
				* (long) UI_SLIDER_WIDTH) / (long) (currentSliderMaxValue
				- (currentSliderMinValue - 1));

	left = UI_SLIDER_CENTER_X - (UI_SLIDER_WIDTH / 2);
	right = left + barLenght;
	top = UI_SLIDER_TOP_Y + 1;
	bottom = UI_SLIDER_TOP_Y + 1 + UI_SLIDER_HEIGHT - 1;

	if (barLenght != 0) {
		colorTable[0] = UI_SLIDER_BAR_BB;
		colorTable[1] = UI_SLIDER_BAR_BB;
		colorTable[2] = UI_SLIDER_BAR_BB;
		colorTable[3] = UI_SLIDER_BAR_BB;
		colorTable[4] = UI_SLIDER_BAR_B;
		colorTable[5] = UI_SLIDER_BAR_B;
		colorTable[6] = UI_SLIDER_BAR;
		colorTable[7] = UI_SLIDER_BAR;
		colorTable[8] = UI_SLIDER_BAR;
		colorTable[9] = UI_SLIDER_BAR;
		colorTable[10] = UI_SLIDER_BAR;
		colorTable[11] = UI_SLIDER_BAR;
		colorTable[12] = UI_SLIDER_BAR_B;
		colorTable[13] = UI_SLIDER_BAR_B;
		HorzGradient(colorTable, left, right, top, 14);
	}

	if (barLenght != UI_SLIDER_WIDTH - 1) {
		colorTable[0] = UI_SLIDER_BAR_BACKGROUND_BB;
		colorTable[1] = UI_SLIDER_BAR_BACKGROUND_BB;
		colorTable[2] = UI_SLIDER_BAR_BACKGROUND_BB;
		colorTable[3] = UI_SLIDER_BAR_BACKGROUND_BB;
		colorTable[4] = UI_SLIDER_BAR_BACKGROUND_B;
		colorTable[5] = UI_SLIDER_BAR_BACKGROUND_B;
		colorTable[6] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[7] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[8] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[9] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[10] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[11] = UI_SLIDER_BAR_BACKGROUND;
		colorTable[12] = UI_SLIDER_BAR_BACKGROUND_B;
		colorTable[13] = UI_SLIDER_BAR_BACKGROUND_B;

		blankLeft = right + 1;
		right = left + UI_SLIDER_WIDTH - 1;
		HorzGradient(colorTable, blankLeft, right, top, 14);
	}

	if (currentSliderShowValue) {
		itoa(currentSliderValue, str, 10);
		LCDPutStr(str, UI_SLIDER_CENTER_X-((strlen(str)*6)/2),
				UI_SLIDER_NUMERIC_VALUE_Y, SMALL, UI_SLIDER_LABEL_FOREGROUND,
				UI_WORKSPACE_BACKGROUND);
	}
}

static int _itoa(int value, char * str, int radix) {
	ldiv_t aux;
	int n;

	if (value == 0)
		return 0;
	aux = ldiv(value, radix);
	n = _itoa(aux.quot, str, radix);
	str[n] = aux.rem > 9 ? aux.rem - 10 + 'A' : aux.rem + '0';
	return n + 1;
}

char *itoa(int value, char * str, int radix) {

	if (value == 0) {
		str[0] = '0';
		str[1] = '\0';
	}
	else {
		if ((radix == 10) && (value < 0)) {
			*str++ = '-';
			value = -value;
		}
		str[_itoa(value, str, radix)] = '\0';
	}
	return str;
}

unsigned int EatSpaceChars(char *str, unsigned int startIdx)
{	while (str[startIdx] == ' ')
		startIdx++;
	return(startIdx);
}

//
// scan through a line of text to find where to break it such that it fits within a
// pixel line width
//		Enter:	S -> string to find line break
//				StringType = ROM_STRING or RAM_STRING
//				StartIdx = index of starting point in string
//				MaxLineWidth = the max width in pixels the line can be
//				Font -> the font
//				StopIdx -> storage to return the index of the last char in string to draw
//				NextStartIdx -> storage to return the index in the string of the next char
//
void BreakLineAtMaxWidth(char *str, unsigned int startIdx, int maxLineWidth,  int font, unsigned int *stopIdx, unsigned int *nextStartIdx)
{	unsigned int idx;
	unsigned int lastBreakIdx;
	int runningTextLength;
	char ch;


	//
	// find the first none space character in the line of text
	//
	idx = startIdx;
	runningTextLength = 0;
	lastBreakIdx = idx;


	//
	// loop to find the first place where the line can be broken
	//
	while(1)
	{	ch = str[idx];

		switch(ch)
		{	case 0:
				*stopIdx = lastBreakIdx;		// found the end of the string, so return
				*nextStartIdx = idx;
				return;

			case '\r':
			case '\n':
				*stopIdx = lastBreakIdx;		// found a line break character, so return
				*nextStartIdx = idx + 1;
				return;
		}
												// add char to running pixel length of string
		runningTextLength += 6;//LCDGetCharacterWidth(C, Font);

		if (ch == ' ')							// check if the line can be broken here
		{	idx++;
			break;
		}

		if (runningTextLength > maxLineWidth)	// check if line is longer than max length
		{	*stopIdx = lastBreakIdx;			// line too long, return last break
			*nextStartIdx = lastBreakIdx + 1;
			return;
		}

		lastBreakIdx = idx++;					// length still OK, try adding next char
	}


	//
	// loop to find the last place where the line can be broken
	//
	while(1)
	{	ch = str[idx];

		switch(ch)
		{	case 0:
				*stopIdx = idx - 1;				// found the end of the string, so return
				*nextStartIdx = idx;
				return;

			case '\r':
			case '\n':
				*stopIdx = idx - 1;				// found a line break character, so return
				*nextStartIdx = idx + 1;
				return;

			case ' ':
				lastBreakIdx = idx - 1;			// found a space, the line can be broken here
				break;
		}
												// add char to running pixel length of string
		runningTextLength += 6;//LCDGetCharacterWidth(C, Font);

		if (runningTextLength > maxLineWidth)	// check if line is longer than max length
		{	*stopIdx = lastBreakIdx;			// line too long, return last break
			*nextStartIdx = EatSpaceChars(str, lastBreakIdx + 1);
			return;
		}

		 idx++;									// length still OK, try adding next char
	}
}



//
// draw a string of text, if the line is wider than the display, break into multiple lines
//		Enter:	S -> string to draw
//				StringType = ROM_STRING or RAM_STRING
//				X = x coord where to draw the string
//				Y = y coord of the middle line
//				LineSpacing = spacing in pixels between each line
//				Justify = JUSTIFY_LEFT, JUSTIFY_CENTER, or JUSTIFY_RIGHT
//				MaxLineWidth = the max width in pixels the line can be
//				Font -> the font
//				ForegroundRGB8 = forground color
//				BackgroundRGB8 = background color
//
void TextBrokenIntoMultipleLines(char *str, int x, int y, int lineSpacing, int justify,
		int maxLineWidth, int font, int foregroundRGB8, int backgroundRGB8)
{
	unsigned int startIdx;
	unsigned int stopIdx;
	unsigned int nextStartIdx;
	char ch;

	startIdx = 0;
	while(1)
	{	ch = str[startIdx];

		if (ch == 0)
			break;

		if (ch == '\r' || ch == '\n')
		{	startIdx = EatSpaceChars(str, startIdx + 1);
			y += 16;
			continue;
		}

		BreakLineAtMaxWidth(str, startIdx, maxLineWidth, font, &stopIdx, &nextStartIdx);
		LCDPutNStr(str, startIdx, stopIdx, x, y, justify, font, foregroundRGB8, backgroundRGB8);
		startIdx = EatSpaceChars(str, nextStartIdx);
		y += lineSpacing;
	}
}


#if 0
//
// determine the number of lines the given text will be after breaking into multiple lines of a max width
//		Enter:	S -> string to measure
//				StringType = ROM_STRING or RAM_STRING
//				MaxLineWidth = the max width in pixels the line can be
//				Font -> the font
//		Exit:	Line count returned
//
byte GetTextLineCountAfterBreakingText(char *S, byte StringType, byte MaxLineWidth, rom byte *Font)
{	unsigned int StartIdx;
	unsigned int StopIdx;
	unsigned int NextStartIdx;
	byte LineCount;
	char C;

	//
	// loop through the text, counting the line breaks
	//
	LineCount = 0;
	StartIdx = 0;
	while(1)
	{	//
		// check if found the end of the string
		//
		C = GetCharFromString(S, StringType, StartIdx);
		if (C == 0)
			return(LineCount);

		if (C == '\r')
		{	StartIdx = EatSpaceChars(S, StringType, StartIdx + 1);
			LineCount++;
			continue;
		}

		BreakLineAtMaxWidth(S, StringType, StartIdx, MaxLineWidth, Font, &StopIdx, &NextStartIdx);
		StartIdx = EatSpaceChars(S, StringType, NextStartIdx);
		LineCount++;
	}
}


#endif
