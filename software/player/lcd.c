#include "lpc214x.h"
#include "startosc.h"
#include "ump3r.h"
#include "sysclk.h"
#include "ssp.h"
#include "lcd.h"

#define LCD_WIDTH 		129
#define LCD_HEIGHT 		131

#define LCD_SPI_FREQ	2000000

extern const unsigned char font6x8[97][8];
extern const unsigned char font8x8[97][8];
//extern const unsigned char font8x16[97][16];
//extern const unsigned char font24x24[5][72];

inline static void LCDCommand(unsigned char data) {
	IOCLR0 = LCD_CS;
	SSPTransferByte(0x00ff & data);
	IOSET0 = LCD_CS;
}

inline static void LCDData(unsigned char data) {
	IOCLR0 = LCD_CS;
	SSPTransferByte(0x100 | (0x00ff & data));
	IOSET0 = LCD_CS;
}

void LCDClear(int color) {
	int i;

	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

#ifdef EPSON
	LCDCommand(PASET);
	LCDData(0);
	LCDData(LCD_HEIGHT);

	LCDCommand(CASET);
	LCDData(0);
	LCDData(LCD_WIDTH);

	LCDCommand(RAMWR);
#endif
#ifdef	PHILLIPS
	LCDCommand(PASETP);
	LCDData(0);
	LCDData(131);

	LCDCommand(CASETP);
	LCDData(0);
	LCDData(131);

	LCDCommand(RAMWRP);
#endif
	for (i = 0; i < (LCD_WIDTH * LCD_HEIGHT) / 2 + 1; i++) {
		LCDData((color >> 4) & 0x00FF);
		LCDData(((color & 0x0F) << 4) | ((color >> 8) & 0x0F));
		LCDData(color & 0x0FF);
	}
}

void LCDInit(void) {
	int j;

	IOSET0 = LCD_CS;
	IODIR0 |= (LCD_CS | LCD_RES);
	for (j = 0; j < 16; j++)
		asm volatile ("nop");
	IOCLR0 = LCD_RES;
	for (j = 0; j < 300000; j++)
		asm volatile ("nop");
	IOSET0 = LCD_RES;
	DelayMs(100);
	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

	LCDCommand(DISCTL); // display control
	LCDData(0x0C);
	LCDData(0x20);
	LCDData(0x0C);

	LCDCommand(COMSCN); // common scanning direction
	LCDData(0x01);

	LCDCommand(OSCON); // internal oscialltor on

	LCDCommand(SLPOUT); // sleep out

	LCDCommand(PWRCTR); // power ctrl
	LCDData(0x0F); //everything on, no external reference resistors

	LCDCommand(DATCTL); // data control
	LCDData(0x00); // 3 correct for normal sin7
	LCDData(0x00); // normal RGB arrangement
	LCDData(0x04); // 16-bit Grayscale Type A

	LCDCommand(VOLCTR); // electronic volume, this is the contrast/brightness
	LCDData(0x29); // volume (contrast) setting - fine tuning, original
	LCDData(0x03); // internal resistor ratio - coarse adjustment

	LCDCommand(NOP);

	LCDCommand(DISON); // display on
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);

}

void LCDContrast(unsigned char setting) {
	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

	LCDCommand(VOLCTR); // electronic volume, this is the contrast/brightness(EPSON)
	LCDData(setting); // volume (contrast) setting - fine tuning, original
	LCDData(0x03); // internal resistor ratio - coarse adjustment
	LCDCommand(SETCON); //Set Contrast(PHILLIPS)
	LCDData(0x30);
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);
}

void LCDSetPixel(int color, unsigned char x, unsigned char y) {
	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

#ifdef EPSON
	LCDCommand(PASET); // page start/end ram
	LCDData(LCD_HEIGHT - y);
	LCDData(LCD_HEIGHT - y);

	LCDCommand(CASET); // column start/end ram
	LCDData(LCD_WIDTH - x);
	LCDData(LCD_WIDTH - x);

	LCDCommand(RAMWR); // write
	LCDData((color >> 4) & 0x00FF);
	LCDData(((color & 0x0F) << 4) | ((color >> 8) & 0x0F));
	LCDData(color & 0x0FF); // nop(EPSON)
#endif
#ifdef	PHILLIPS
	LCDCommand(PASETP); // page start/end ram
	LCDData(x);
	LCDData(ENDPAGE);

	LCDCommand(CASETP); // column start/end ram
	LCDData(y);
	LCDData(ENDCOL);

	LCDCommand(RAMWRP); // write

	LCDData((unsigned char)((color>>4)&0x00FF));
	LCDData((unsigned char)(((color&0x0F)<<4)|0x00));
#endif
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);
}

void LCDFilledRectangle(int left, int top, int right, int bottom, int color) {
	int i, pixelCount;

	if ((left > right) || (top > bottom))
		return;

	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

	LCDCommand(PASET);
	LCDData(LCD_HEIGHT - bottom);
	LCDData(LCD_HEIGHT - top);

	LCDCommand(CASET);
	LCDData(LCD_WIDTH - right);
	LCDData(LCD_WIDTH - left);

	LCDCommand(RAMWR);

	pixelCount = (right - left + 1) * (bottom - top + 1);

	for (i = 0; i < pixelCount; i++) {
		LCDData((color >> 4) & 0x00FF);
		LCDData(((color & 0x0F) << 4) | ((color >> 8) & 0x0F));
		LCDData(color & 0x0FF);
	}
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);
}

void LCDPutChar(char c, int x, int y, int size, int fColor, int bColor) {
	int i, j;
	unsigned int nCols;
	unsigned int nRows;
	unsigned int nBytes;
	unsigned char PixelRow;
	unsigned int Word0;
	unsigned int Word1;
	unsigned char *pFont = 0;
	unsigned char *pChar;

	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

	if (size == SMALL)
		pFont = (unsigned char *) font6x8;
	if (size == MEDIUM)
		pFont = (unsigned char *) font8x8;
	if (size == LARGE)
		//		pFont = (unsigned char *) FONT8x16;
		pFont = (unsigned char *) font8x8;
	if (size == BIG)
		//		pFont = (unsigned char *) FONT24x24;
		pFont = (unsigned char *) font8x8;

	nCols = *pFont;
	nRows = *(pFont + 1);
	nBytes = *(pFont + 2);

	pChar = pFont + (nBytes * (c - 0x1F)) + nBytes - 1;
	LCDCommand(PASET);
	LCDData(LCD_HEIGHT - (y + nRows - 1));
	LCDData(LCD_HEIGHT - (y));

	LCDCommand(CASET);
	LCDData(LCD_WIDTH - (x + nCols - 1));
	LCDData(LCD_WIDTH - (x));

	LCDCommand(RAMWR);

	for (i = 0; i < nBytes; i++) {
		PixelRow = *pChar--;
		for (j = 0; j < nCols / 2; j++) {
			Word0 = ((PixelRow & 0x1) != 0) ? fColor : bColor;
			PixelRow >>= 1;
			Word1 = ((PixelRow & 0x1) != 0) ? fColor : bColor;
			PixelRow >>= 1;
			LCDData((Word0 >> 4) & 0xFF);
			LCDData(((Word0 & 0xF) << 4) | ((Word1 >> 8) & 0xF));
			LCDData(Word1 & 0xFF);
		}
	}
	LCDCommand(NOP);
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);
}

void LCDPutCharEx(char c, int x, int y, int offsetX, int last, int size,
		int fColor, int bColor) {
	int i, j;
	unsigned int nCols;
	unsigned int nRows;
	unsigned int nBytes;
	unsigned char PixelRow;
	unsigned int Word0;
	unsigned int Word1;
	unsigned char *pFont = 0;
	unsigned char *pChar;

	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);
	if (size == SMALL)
		pFont = (unsigned char *) font6x8;
	if (size == MEDIUM)
		pFont = (unsigned char *) font8x8;
	if (size == LARGE)
		//		pFont = (unsigned char *) FONT8x16;
		pFont = (unsigned char *) font8x8;
	if (size == BIG)
		//		pFont = (unsigned char *) FONT24x24;
		pFont = (unsigned char *) font8x8;

	nCols = *pFont;
	nRows = *(pFont + 1);
	nBytes = *(pFont + 2);

	nCols -= (offsetX + (8 - last));

	pChar = pFont + (nBytes * (c - 0x1F)) + nBytes - 1;
	LCDCommand(PASET);
	LCDData(LCD_HEIGHT - (y + nRows - 1));
	LCDData(LCD_HEIGHT - (y));

	LCDCommand(CASET);
	LCDData(LCD_WIDTH - (x + nCols - 1));
	LCDData(LCD_WIDTH - (x));

	LCDCommand(RAMWR);

	for (i = 0; i < nBytes; i++) {
		PixelRow = *pChar--;
		PixelRow >>= (8 - last);
		for (j = 0; j < nCols / 2; j++) {
			Word0 = ((PixelRow & 0x1) != 0) ? fColor : bColor;
			PixelRow >>= 1;
			Word1 = ((PixelRow & 0x1) != 0) ? fColor : bColor;
			PixelRow >>= 1;
			LCDData((Word0 >> 4) & 0xFF);
			LCDData(((Word0 & 0xF) << 4) | ((Word1 >> 8) & 0xF));
			LCDData(Word1 & 0xFF);
		}
	}
	LCDCommand(NOP);
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);
}

void LCDPutStr(const char *pString, int x, int y, int Size, int fColor,
		int bColor) {
	while (*pString) {
		LCDPutChar(*pString++, x, y, Size, fColor, bColor);
		if (Size == SMALL)
			x = x + 6;
		if (Size == MEDIUM)
			x = x + 8;
		if (Size == LARGE)
			x = x + 8;
		if (Size == BIG)
			x = x + 24;
		if (x > LCD_WIDTH)
			break;
	}
}

void LCDPutStrEx(const char *pString, int x, int y, int startOffset,
		int rightEnd, int Size, int fColor, int bColor) {
	int last;

	startOffset &= ~0x1;
	rightEnd = rightEnd + ((rightEnd - startOffset) % 2);
	startOffset *= 2;
	pString += (startOffset / 8);
	LCDPutCharEx(*pString++, x, y, startOffset % 8, 8, Size, fColor, bColor);
	x += (8 - (startOffset % 8));
	while (*pString) {
		LCDPutChar(*pString++, x, y, Size, fColor, bColor);
		if (Size == SMALL)
			x = x + 6;
		if (Size == MEDIUM)
			x = x + 8;
		if (Size == LARGE)
			x = x + 8;
		if (Size == BIG)
			x = x + 24;
		if ((x > LCD_WIDTH) || (x + 8 > rightEnd))
			break;
	}
	if (pString) {
		last = (rightEnd - x);
		if (last)
			LCDPutCharEx(*pString, x, y, 0, last, Size, fColor, bColor);
	}
}

//
// draw a string between the start and stop indexes, using RGB8 colors
//		Enter:	S -> string to draw
//				StringType = ROM_STRING or RAM_STRING
//				StartIdx = index in string of first character to draw
//				StopIdx = index in string of last character to draw
//				X, Y = coord of where to draw lower left corner of char (including space
//				  for the possible decender)
//				Justify = JUSTIFY_LEFT, JUSTIFY_CENTER, or JUSTIFY_RIGHT
//				Font -> the font
//				ForegroundRGB8 = forground color
//				BackgroundRGB8 = background color
//		Exit:	the X coordinate right of the character just drawn is returned
//
void LCDPutNStr(char *S, unsigned int StartIdx, unsigned int StopIdx, int X,
		int Y, int Justify, int Font, int ForegroundRGB8, int BackgroundRGB8) {
	char C;
	int i;

	//
	// adjust the X coord if justifying right or center
	//
	switch (Justify) {
	case JUSTIFY_CENTER:
		X -= ((6 * ((StopIdx - StartIdx) + 1)) / 2);
		//LCDGetStringWidthBetweenIndexes(S, StringType, StartIdx, StopIdx, Font) / 2;
		break;

	case JUSTIFY_RIGHT:
		X -= ((6 * ((StopIdx - StartIdx) + 1)) - 1);
		//(LCDGetStringWidthBetweenIndexes(S, StringType, StartIdx, StopIdx, Font) - 1);
		break;
	}

	//
	// determine if the string is saved in ROM or RAM, the draw it
	//
	for (i = StartIdx; i <= StopIdx; i++) {
		C = S[i];
		if (C == 0)
			break;
		LCDPutChar(C, X, Y, Font, ForegroundRGB8, BackgroundRGB8);
		if (Font == SMALL)
			X = X + 6;
		if (Font == MEDIUM)
			X = X + 8;
		if (Font == LARGE)
			X = X + 8;
		if (Font == BIG)
			X = X + 24;
	}
}

void LCDBitmap(const unsigned char *Bitmap, int X, int Y)
{	int Width;
	int Height;
	int ByteCount, i;

	Width = Bitmap[1];							// get the size of the bitmap
	Height = Bitmap[2];
	ByteCount = Bitmap[3] + ((int)Bitmap[4] << 8);

	SSPSetMode(SSP_CR0_DSS_9, SSP_CR0_FRF_SPI, LCD_SPI_FREQ);

	LCDCommand(PASET);
	LCDData(LCD_HEIGHT - ((Y + Width)-1));
	LCDData(LCD_HEIGHT - Y);

	LCDCommand(CASET);
	LCDData(LCD_WIDTH - ((X + Height)-1));
	LCDData(LCD_WIDTH - X);

	LCDCommand(RAMWR);

	Bitmap += 5;
	for (i = 0; i < ByteCount; i++) {
		LCDData(*Bitmap++);
	}
	LCDCommand(NOP);
	SSPSetMode(SSP_CR0_DSS_8, SSP_CR0_FRF_SPI, 20000000);

}


void LCDBackLight(int duty) {
	int pclk;

	PINSEL1 &= ~(0x3 << 10);
	PINSEL1 |= (0x1 << 10);

	pclk = GetPclk();
	PWMTCR = (1 << 1); // reset
	PWMPR = 0; /* count frequency:Fpclk */
	PWMMR0 = pclk / 20000;
	PWMMCR |= 1 << 1; //reset on PWMMR0, reset
	PWMPCR = 1 << 13;
	PWMTCR = (1 << 0) | (1 << 3); /* counter enable, PWM enable */
	PWMMR5 = ((pclk / 20000) * (duty)) / 100;
	PWMLER = 1 << 5;
}
