/*
 * ump3r.h
 *
 *  Created on: 16 Feb 2011
 *      Author: psampaio
 */

#ifndef UMP3R_H_
#define UMP3R_H_

// SD - GPIO 0
#define SD_CD					(1 << 16)
#define SD_CS					(1 << 20)


// LCD - GPIO 0
#define LCD_RES 				(1<<13)
#define LCD_CS					(1<<22)
#define LCD_ON					(1<<21)
#define LCD_DIO					(1<<19)
#define LCD_SCK 				(1<<17)

// LEDs - GPIO 1
#define LED_BLUE				(1 << 21)
#define LED_RED					(1 << 22)
#define LED_GREEN				(1 << 23)

// JOYSTICK - GPIO 1 (except center GPIO 0)
#define JOYSTICK_UP				(1 << 19)
#define JOYSTICK_DOWN			(1 << 18)
#define JOYSTICK_LEFT			(1 << 20)
#define JOYSTICK_RIGHT			(1 << 17)

#define JOYSTICK_CENTER			(1 << 15)

// MP3
#define SCLK_PINSEL				(1 << 8)		// Select SPI Function for P0.4
#define MISO_PINSEL				(1 << 10)		// Select SPI Function for P0.5
#define MOSI_PINSEL				(1 << 12)		// Select SPI Function for P0.6

#define MP3_XCS					(1 << 7)			// P0.7
#define MP3_XDCS				(1 << 2)			// P0.2
#define MP3_DREQ				(1 << 3)			// P0.3

#define MP3_RX					(1 << 8)			// P0.8

#define MP3_XRES				(1 << 16)			// Define P1.16

#endif /* UMP3R_H_ */
