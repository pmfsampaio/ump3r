/*
 * sysclk.h
 *
 *  Created on: 16 Feb 2011
 *      Author: psampaio
 */

#ifndef SYSCLK_H_
#define SYSCLK_H_

int SCKInit(void);

unsigned int GetTicks(void);
inline unsigned int GetElapsedTicks(unsigned int startTime);
void DelayMs(unsigned int duration);
void Delay100us(unsigned int duration);

#define _MS(_v)		(_v * 10)

int SetSecond(int match);
unsigned int GetSeconds(void);


#endif /* SYSCLK_H_ */
