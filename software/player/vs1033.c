/*
 * vs1033.c
 *
 *  Created on: 13 Mar 2011
 *      Author: psampaio
 */
#include "lpc214x.h"
#include "ump3r.h"
#include "sysclk.h"
#include "vs1033.h"

static inline void mp3_waitReady(unsigned char cnt) {
	unsigned char i;

	for (i = 0; i < cnt; i++) {
		Delay100us(2);
		if ((IOPIN0 & MP3_DREQ) == 0)
			break;
	}
	while ((IOPIN0 & MP3_DREQ) == 0)
		;
}

#define SPIF	(1<<7)

static inline unsigned char mp3_spi(unsigned char data) {
	S0SPDR = data;
	while ((S0SPSR & SPIF) == 0)
		;
	return S0SPDR;
}

void mp3_writeReg(mp3_register_t addr, unsigned short data) {
	IOCLR0 = MP3_XCS;
	mp3_spi(0x02);//0b00000010); // write command
	mp3_spi(addr); // send address
	mp3_spi((data & 0xFF00) >> 8); // send data MSB first
	mp3_spi(data & 0x00FF);

	IOSET0 = MP3_XCS;

	mp3_waitReady(1);
}

void VS1033Init(void) {
	unsigned long spick = 60;//((60000000 / 1000000) >> 1);
	// note that this must be called after the SD card has initialized to ensure SPI has initialized

	// setup all pin directions
	IOCLR1 = MP3_XRES;
	IODIR1 |= MP3_XRES;

	IOSET0 = MP3_XCS | MP3_RX;
	IOSET0 = MP3_XDCS;

	IODIR0 |= MP3_XDCS | MP3_XCS | MP3_RX;
	IODIR0 &= ~MP3_DREQ;

	PINSEL0 &= ~((3 << 6) | (3 << 8) | (3 << 10) | (3 << 12));
	PINSEL0 |= (SCLK_PINSEL | MISO_PINSEL | MOSI_PINSEL);
	S0SPCR = 0x20; // Master, no interrupt enable, 8 bits
	spick &= 0x0FF;
	S0SPCCR = (spick & 0x0FF);

	Delay100us(2);
	IOSET1 = MP3_XRES;

	mp3_waitReady(10);

	mp3_writeReg(MP3_REG_MODE, 0x0803);//0b0000 1000 0000 0011); // see datasheet
	mp3_writeReg(MP3_REG_CLOCKF, 0x1800 | 0xE000); // see datasheet
}

void VS1033Play(unsigned char* buffer, unsigned char length) {
	unsigned char i;

	while ((IOPIN0 & MP3_DREQ) == 0)
		;
	IOCLR0 = MP3_XDCS;
	for (i = 0; i < length; i++) {
		mp3_spi(buffer[i]);
	}
	IOSET0 = MP3_XDCS;
}

void VS1033PlayEx(unsigned char* buffer, unsigned length) {
	unsigned int i, j, max;

	for (j = 0; j < length;) {
		max = (j+32 < length) ? 32 : length-j;
		while ((IOPIN0 & MP3_DREQ) == 0)
			;
		IOCLR0 = MP3_XDCS;
		for (i = 0; i < max; i++) {
			mp3_spi(buffer[j+i]);
		}
		IOSET0 = MP3_XDCS;
		j += 32;
	}
}

void VS1033Stop(void) {
	unsigned int i, j;

	for (j = 0; j < 2048;) {
		while ((IOPIN0 & MP3_DREQ) == 0)
			;
		IOCLR0 = MP3_XDCS;
		for (i = 0; i < 32; i++) {
			mp3_spi(0);
		}
		IOSET0 = MP3_XDCS;
		j += 32;
	}
}

void VS1033Volume(unsigned char vol)
{
    mp3_writeReg(MP3_REG_VOL, ((int)vol) << 8 | vol);
}
