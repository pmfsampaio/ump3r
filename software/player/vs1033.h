/*
 * vs1033.h
 *
 *  Created on: 13 Mar 2011
 *      Author: psampaio
 */

#ifndef VS1033_H_
#define VS1033_H_




typedef enum
{
    MP3_REG_MODE = 0x00,
    MP3_REG_CLOCKF = 0x03,
    MP3_REG_VOL = 0x0B,
}
mp3_register_t;

typedef enum
{
    PLAYSTATUS_OK,
    PLAYSTATUS_BUSY,
    PLAYSTATUS_ENDOFFILE,
}
mp3_playStatus_t;

void VS1033Init(void);
void VS1033Stop(void);
void VS1033Volume(unsigned char vol);
void VS1033Play(unsigned char* buffer, unsigned char length);
void VS1033PlayEx(unsigned char* buffer,unsigned length);

#endif /* VS1033_H_ */
