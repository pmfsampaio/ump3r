#include <time.h>
#include "lpc214x.h"
#include "startosc.h"
#include "rtc.h"

void RTCInit(void) {
	AMR = ~0x00;
	CIIR = 0x01;
	CCR |= CCR_CTCRST;
	ILR = ILR_RTCCIF | ILR_RTCALF;
	PREINT = PREINT_RTC;
	PREFRAC = PREFRAC_RTC;
	CCR = CCR_CLKEN | CCR_CLKSRC; // disable CTC-Reset and enable clock with CLKEN
	PCONP &= ~(1UL << 9);		// connect to 32kHz
}

void RTCStart(void) {
	CCR |= CCR_CLKEN;
	ILR = ILR_RTCCIF | ILR_RTCALF;
}

void RTCStop(void) {
	CCR &= ~CCR_CLKEN;
}

void RTC_CTCReset(void) {
	CCR |= CCR_CTCRST;
}

void RTCSetTime(struct tm *time) {
	CCR &= ~CCR_CLKEN;
	SEC = time->tm_sec;
	MIN = time->tm_min;
	HOUR = time->tm_hour;
	DOM = time->tm_mday;
	DOW = time->tm_wday;
	DOY = time->tm_yday;
	MONTH = time->tm_mon;
	YEAR = time->tm_year;
	CCR |= CCR_CTCRST;
	ILR = ILR_RTCCIF | ILR_RTCALF;
	CCR = CCR_CLKEN | CCR_CLKSRC;
}

void RTCGetTime(struct tm *time) {
	time->tm_sec = SEC;
	time->tm_min = MIN;
	time->tm_hour = HOUR;
	time->tm_mday = DOM;
	time->tm_wday = DOW;
	time->tm_yday = DOY;
	time->tm_mon = MONTH;
	time->tm_year = YEAR;
}

int RTCUpdated(void)
{
	int res = ILR;
	ILR = res;
	return res;
}
